"use client";

import { Suspense, Fragment, useEffect, useState } from "react";
import Highlight, { Prism, defaultProps } from "prism-react-renderer";
import { CopyIcon } from "./icons/CopyIcon";
import { CheckIcon } from "./icons/CheckIcon";

(typeof global !== "undefined" ? global : window).Prism = Prism;

const ExtraLanguages = Promise.all([import("prismjs/components/prism-php")]);
const copyIconClassNames = "h-3 w-3 fill-neutral-500  dark:fill-neutral-400";

export function CodeBlock({ children, language }) {
  const [copied, setCopied] = useState(false);

  useEffect(() => {
    const loadLanguages = async () => {
      await ExtraLanguages;
    };

    loadLanguages();
  });

  const copyToClipboard = () => {
    navigator.clipboard.writeText(children.trimEnd());

    setCopied(true);

    setTimeout(() => {
      setCopied(false);
    }, 2000);
  };

  return (
    <Highlight
      {...defaultProps}
      code={children.trimEnd()}
      language={language}
      theme={undefined}
    >
      {({ className, style, tokens, getTokenProps }) => (
        <div className="relative">
          <div
            className="pointer absolute right-[9px] top-[9px] cursor-pointer rounded border border-neutral-300 p-2 dark:border-neutral-700"
            onClick={copyToClipboard}
          >
            {copied ? (
              <CheckIcon className={copyIconClassNames} />
            ) : (
              <CopyIcon className={copyIconClassNames} />
            )}
          </div>

          <pre className={`${className} relative block pr-12`} style={style}>
            <code>
              {tokens.map((line, lineIndex) => (
                <Fragment key={lineIndex}>
                  {line
                    .filter(token => !token.empty)
                    .map((token, tokenIndex) => (
                      <span key={tokenIndex} {...getTokenProps({ token })} />
                    ))}
                  {"\n"}
                </Fragment>
              ))}
            </code>
          </pre>
        </div>
      )}
    </Highlight>
  );
}

export function Fence({ children, language }) {
  return (
    <Suspense fallback={<pre>{children}</pre>}>
      <CodeBlock language={language}>{children}</CodeBlock>
    </Suspense>
  );
}
