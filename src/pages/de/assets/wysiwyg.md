---
title: Texteingabefelder
description: Erfahren Sie mehr über TinyMCE, den Texteditor für HTML-Inhalte.
meta:
  - name: og:title
    content: Texteingabefelder
  - name: og:description
    content: Erfahren Sie mehr über TinyMCE, den Texteditor für HTML-Inhalte.
---

## Kurze Erklärung zu den Texteingabefeldern

Die Oberfläche des Texteingabefeldes bietet die gleichen Möglichkeiten wie andere Textbearbeitungsprogramme.

Sie können verschiedene Headlines erstellen, kursiv, fett oder unterstrichen und Aufzählungen benutzen. Auch Verlinkungen können hier erstellt werden.

## Menüleiste

### Vor & Zurück

Die beiden Pfeile machen eine Eingabe entweder rückgängig oder stellen sie wieder her.

![](/images/assets/wysiwyg-texteditor-arrows.jpg)

### Headlines

Bei der Bearbeitung ihres Eintrages haben Sie die Wahl zwischen drei Headlines unterschiedlicher Größe und einfachem Text.

![](/images/assets/wysiwyg-texteditor-heading-2.jpg)

Bei der Bearbeitung ihrer Seiten haben Sie die Wahl zwischen Text (paragraph), sechs Headlines unterschiedlicher Größen und preformatted Text (der Text erscheint genauso, wie Sie ihn eingeben).

![](/images/assets/wysiwyg-texteditor-heading-1.jpg)

### Text Styles

Sie können zwischen fettgedrucktem, kursiven und unterstrichenem Text wählen.

![](/images/assets/wysiwyg-texteditor-bold.jpg)

### Aufzählungen / Listen

Ihnen stehen verschiedene Aufzählungssymbole und Zeichen wie z.B. Kreis, Punkt, Nummer oder Großbuchstaben zur Auswahl.

![](/images/assets/wysiwyg-texteditor-list-1.jpg) ![](/images/assets/wysiwyg-texteditor-list-2.jpg)

### Links einfügen

Durch Klicken auf das kleine Kettensymbol können Sie einen Link ihrer Wahl einfügen.

![](/images/assets/wysiwyg-texteditor-link-1.jpg)

**Schritte:**

![](/images/assets/wysiwyg-texteditor-link-2.jpg)

1. Klicken Sie auf das Kettensymbol.
1. Ein neue Maske öffnet sich.
1. Tragen Sie hier die URL des Links ein, die URL ist auch gleichzeitig der Text, der angezeigt wird.
1. Sie können dem Link noch einen Titel geben.
1. In dem Feld 'Target' können Sie entscheiden, ob bei Klicken des Links ein neues Fenster (new window) geöffnet werden soll oder ob der Link im aktuellen Fenster (none) geöffnet werden soll.
1. Klicken Sie `OK` und der Link wird in ihrem Text angezeigt. Bei Klicken auf `Abbrechen` wird alles verworfen.

{% callout type="warning" title="Besonderheiten" %}
- Tragen Sie ihren Text (paragraph) als Fließtext ein, der Zeilenumbruch passiert hier automatisch.
- Durch drücken der Enter-Taste erstellen Sie ein neues Textfeld.
{% /callout %}
