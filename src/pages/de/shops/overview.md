---
title: Übersicht
meta:
  - name: description
    content: Erfahren Sie, welche Informationen Ihnen auf der Übersichtseite Ihres Online-Shops in der Händler-Verwaltung angezeigt werden und was diese bedeuten.
  - name: og:title
    content: Übersicht
---

In der Übersicht, oder auch Dashboard genannt, finden Sie alle umsatzrelevanten Informationen zu Ihrem Shop.

Über den Button `zum Online-Shop` gelangen Sie zu Ihrem Shop, der Button `Bestellungen ansehen` bringt Sie direkt zu Ihrer Bestellübersicht.

## Umsatz

Das Säulendiagramm zeigt Ihnen alle Umsätze der letzten 14 Tage. Jede Säule steht für einen Tag dieses Zeitraums.

## Shopping Statistiken

Die Shopping Statistiken geben Ihnen einen Eindruck über die Bestellungen und Umsätze der letzten 14 Tage und die Tendenzen zu den vorherigen Zeiträumen.

## Provisionsabrechnungen

Hier ist ein Überblick über Ihre Provisionsabrechnungen mit Rechnungsnummer, Rechnungsdatum, Rechnungsbetrag Netto und dem Provisionsbetrag. Mit einem Klick auf das kleine PDF Symbol rechts können Sie sich die Rechnung als PDF herunterladen und ausdrucken.  
