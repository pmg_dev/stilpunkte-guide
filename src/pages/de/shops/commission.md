---
title: Provision
meta:
  - name: og:title
    content: Provision
---

Hier sehen Sie die Auflistung der kommissionierten Käufe. {% .lead %}

Alle 14 Tage erhalten Sie eine Rechnung von der STILPUNKTE GmbH über ihre Verkäufe. Die Rechnungen enthalten eine Auflistung der verkauften Produkte, den Netto Preise, des Provisionssatzes und der Endsumme, die Sie an die STILPUNKTE GmbH zahlen müssen.

Die Rechnungen werden Ihnen auch per Mail zugestellt.
