---
title: Kategorien
description: Erfahren Sie, wie sie die Kategorien in Ihrem Online-Shop anpassen und personalisieren können.
meta:
  - name: keywords
    content: Stilpunkte, Shopping-Mall, Kategorien, Ändern, Bearbeiten
  - name: og:title
    content: Kategorien
---

Kategorien gliedern Ihren Online-Shop in spezialisierte Bereiche und sortieren Ihre Produkte, um dem Kunden den Einstieg zu erleichtern und die passenden Produkte nach seinen Bedürfnissen zu finden. Die Kategorien bilden im Online-Shop Ihr Menü, das im Header erscheint, deswegen ist es sehr wichtig, dass Sie Ihre Produkte genau kategorisieren. {% .lead %}

{% callout type="warning" title="Hinweis" %}
  Die Reihenfolge der Kategorien ist nicht veränderbar, Sie erfolgt alphabetisch.

  Kategorien erscheinen in der Übersicht erst dann, wenn Produkten eine passende Kategorie zugeordnet wurde. Anschließend können diese nach Ihren Bedürfnissen angepasst werden.
{% /callout %}

## Übersicht

{% callout title="Tipp!" %}
Über den Button `zum Online-Shop` gelangen Sie direkt auf Ihren STILPUNKTE® Online-Shop.
{% /callout %}

Hier sehen Sie eine Übersicht über Ihre Seiten im Online-Shop. Haben Sie noch keine Produkte in Kategorien eingeordnet oder Sie starten gerade erst mit der Einrichtung Ihres Online-Shops, sehen Sie hier nur die `Home` Seite.
Bei bereits eingestellten und kategorisierten Produkten, erscheinen unter der `Home` Seite die weiteren Kategorien, welche die Unterseiten Ihres Online-Shops bilden.

Im unteren Bild sehen Sie die Übersicht eines Shops, der bereits diverse Produkte mit Kategorien versehen hat.

![](/images/shops//categories-overview-1.jpg)

Alle Produkte wurden in diesem Beispiel in der Kategorie `2` Möbel einsortiert. Die Kategorie `3` Sortierung wird mit dem eingerückten Pfeil dargestellt und bedeutet, dass diese Seiten Unterseiten des Menüpunktes `Möbel` sind.
Da dieser Online-Shop nur eine Kategorie `2` hat, wird das Menü nachher nur aus den Kategorie `3` Begriffen bestehen.

Sobald Sie verschiedene Kategorien `2` verwenden, erscheinen diese im Menü und die Kategorie `3` Sortierungen werden zu einem Untermenü.

## Schritte

1. Klicken Sie in der Übersicht auf das kleine Icon `Schraubenschlüssel` rechts von der Seite, die Sie bearbeiten wollen.
1. Ändern Sie je nach Wunsch den Titel der Seite.
1. Geben Sie mit Hilfe des Text Editors eine Beschreibung der Seite ein.
1. Legen Sie Keywords (Schlagwörter) für die Seite an.
1. Legen Sie eine Meta Beschreibung an.
1. Laden Sie ein Foto hoch, indem Sie in das Feld `Foto hier ablegen` klicken und ein Foto von Ihrem Computer hochladen oder Sie ziehen es per Drag & Drop in das Feld `Foto hier ablegen`.
1. Mit einem Klick auf den Button `Übernehmen` speichern Sie Ihre Änderungen, bleiben aber in dem Fenster und können weiter bearbeitet werden.
1. Mit einem Klick auf `Speichern & Schließen` speichern Sie Ihre Änderungen und kehren zur Übersicht zurück.

{% callout type="warning" title="Hinweis" %}
Mit einem erden und verloren gehen.
{% /callout %}

## Bearbeiten

Sie haben die Möglichkeit alle Seiten im Menü zu bearbeiten.

{% callout type="warning" title="Hinweis" %}
  Die Bearbeitungsmaske ist für alle Seiten gleich.
{% /callout %}

### Titel

Sie können den Titel Ihrer `Home` Seite oder Kategorie nach Belieben ändern. Dieser erscheint anschließend in der Menü-Übersicht, sowie als Überschrift über dem Produkt-Listing.

Ändern Sie den Namen einer Kategorie, betrifft dies nur Ihren eigenen Online-Shop, nicht aber den STILPUNKTE® Online-Portal.

### Beschreibung

Sie können einen Beschreibungstext hinterlegen, dieser wird unten auf der Seite nach dem Produkt-Listing angezeigt.

### Meta Keywords

Beschreiben Sie Ihre Seite in kurzen Schlagworten. Diese werden in der internen Suche berücksichtigt und kann Ihr Suchmaschinen Ranking verbessern.

Trennen Sie jedes Keyword mit einem `,` um ein neues Keyword einzugeben.

### Meta Beschreibung

Beschreiben Sie Ihre Seite in einer kurzen Zusammenfassung, diese sollte nicht länger als **155 Zeichen** sein und kann Ihr Suchmaschinen Ranking verbessern. Außerdem wird die Meta-Beschreibung auch auf der Suchmaske (z.B. bei der Google Suche) angezeigt.

### Foto hochladen

Sie können für jede Seite ein Startbild hochladen, es sollte `max. 1920 x 1080 px` groß sein. Es erscheint direkt im oberen Bereich der Seite unter dem Menü.

## Bedingungen

- Alle Pflichtfelder sind mit einem `*` versehen.
- Geben Sie der Seite eine aussagekräftige META Beschreibung und Keywords und versuchen Sie, sich an die vorgegeben Länge der Zeichen zu halten.
- Halten Sie sich an die vorgegebene optimale Größe der Fotos.
