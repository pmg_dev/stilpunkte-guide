---
title: AGB
meta:
  - name: description
    content: Allgemeine Geschäftsbedingungen der PROMEDIAGROUP GmbH für das STILPUNKTE Shopping-Mall
  - name: keywords
    content: Allgemeine Geschäftsbedingungen, Shopping-Mall, Stilpunkte
  - name: og:title
    content: Allgemeine Geschäftsbedingungen
---
# ALLGEMEINE GESCHÄFTSBEDINGUNGEN der PROMEDIAGROUP GmbH für die STILPUNKTE Shopping-Mall

## 1. Gegenstand und Geltungsbereich

1.1 Diese Allgemeinen Geschäftsbedingungen (AGB) gelten für alle Geschäftsbeziehungen zwischen der PROMEDIAGROUP GmbH, Max-Planck-Straße 6-8, 50858 Köln („PROMEDIA“) und den PREMIUM-PARTNERN („KUNDE“) im Rahmen der Nutzung des STILPUNKTE SHOPPING-MALL („DIENST“) unter der URL [www.stilpunkte.de/shops](https://www.stilpunkte.de/shops/).

1.2 Der DIENST ist ein virtueller Shopping-Center, der sich an das typische STILPUNKTE-Publikum (Nutzer mit höherem Haushaltseinkommen) richtet und ihnen in einem Online-Portal vereint ausgewählte erstklassige Waren und/oder Dienstleistungen („Produkte“) der KUNDEN anbietet. Die Zusammenführung der verschiedenen Produktpaletten der KUNDEN auf einer Online-Plattform bietet den KUNDEN die Möglichkeit dem exklusiven Nutzerkreis ihre hochwertigen Produkte wie bei einer Shopping-Tour in einem Luxus- Center anzubieten und .

1.3 Die Teilnahme am DIENST ist ausschließlich KUNDEN vorbehalten. / Für die Teilnahme am DIENST ist eine PROMEDIA PREMIUM - Mitgliedschaft erforderlich.

1.4 Entgegenstehende allgemeine Geschäftsbedingungen oder sonstige allgemeine Vertragsbedingungen der KUNDEN haben keine Gültigkeit.

## 2. Vertragsabschluss

2.1 Die Teilnahme am DIENST kann der KUNDE ganz einfach in seinem Kundenaccount als zusätzliche Leistung dazu buchen. Die Buchung im Kundenaccount stellt das Vertragsangebot des KUNDEN dar.

2.3 Ein Vertrag kommt erst durch die ausdrückliche Annahme durch PROMEDIA zustande.  

2.3 PROMEDIA ist zur Annahme der Buchung nicht verpflichtet.

## 3. Leistungen von PROMEDIA

3.1 PROMEDIA bietet seinen KUNDEN eine Online-Plattform im Stile einer Shopping-Mall auf der sich die KUNDEN präsentieren und ihren Webshop einbinden können. KUNDEN, die über keinen eigenen Webshop verfügen stellt PROMEDIA einen Webshop zur Verfügung.

3.2 Der Verkauf von Produkten erfolgt durch die KUNDEN selbst. PROMEDIA wird nicht Vertragspartner bei den im Rahmen der Nutzung des DIENSTES geschlossenen Verträgen mit den Nutzern des Dienstes.

3.3 Alle im Rahmen der Teilnahme am DIENST hochgeladenen Produkte werden von PROMEDIA mit folgenden Marketingmaßnahmen zusätzlich beworben:

3.4 PROMEDIA wählt aus allen im Rahmen des DIENSTES von den KUNDEN angebotenen Waren sogenannte „Top Produkte“ aus, die in dem DIENST und auf den Social-Media-Kanälen von PROMEDIA und dem Printmagazin „STILPUNKTE“ exponiert als „Top Produkt“ beworben werden. Die Auswahl der „Top Produkte“ bleibt PROMEDIA vorbehalten. Der KUNDE hat keinen Anspruch darauf, dass aus seinem Angebot bestimmte Produkte als „Top Produkte“ ausgewählt werden.
Die Auswahl der „Top Produkte“ erfolgt nach folgenden Kriterien:

3.5 Die technische Bereitstellung des Dienstes erfolgt durch PROMEDIA auf dem derzeit aktuellen Stand der Technik. PROMEDIA strebt eine Erreichbarkeit des DIENSTES von nicht unter 99% im Jahresmittel an. Störungen und Beeinträchtigungen des Internetverkehrs liegen allerdings in der Regel nicht im Einflussbereich von PROMEDIA und können daher nicht völlig ausgeschlossen werden.  Ausgenommen davon sind Störungen des DIENSTES die nicht im Einflussbereich von PROMEDIA liegen (insbesondere bei Stromausfällen und Störungen der Telekommunikationsnetze, in Fällen höherer Gewalt oder sonstigen Fällen, in denen die Ursache der Nichtverfügbarkeit außerhalb des Verantwortungsbereichs von PROMEDIA liegt) sowie die Nichtverfügbarkeit des DIENSTES wegen routinemäßiger Wartungs- oder Aktualisierungsmaßnahmen oder wegen rechtzeitig angekündigter erforderlicher Wartungen.

## 4. Pflichten und Obliegenheiten des KUNDEN

4.1 Der DIENST richtet sich an einen exklusiven Nutzerkreis (Personen mit höherem Haushaltseinkommen). Deswegen ist der KUNDE verpflichtet, den gehobenen Anspruch des DIENSTES sowohl hinsichtlich der Qualität und Aufmachung der dort angebotenen Produkte, als auch im Hinblick auf den Umgang mit den Nutzern in Bezug auf Kundenservice, Kundenreaktionsmanagement und Problemlösung gerecht zu werden. PROMEDIA behält sich vor, bestimmte Produkte, die nach Art oder Präsentation nicht dem Standard des DIENSTES entsprechen, vom DIENST auszuschließen.

4.2 Der KUNDE versichert, dass er im Besitz aller erforderlichen Nutzungsrechte für die von ihm im Rahmen des DIENSTES verwendeten Texte und Bilder ist. Der KUNDE stellt PROMEDIA van allen Ansprüchen Dritter im Zusammenhand mit Schutzrechtsverletzungen durch das Angebot seiner Produkte im Rahmen des DIENSTES frei.

4.3 Der KUNDE überträgt PROMEDIA die zur öffentlichen Wiedergabe auf der Website und im STILPUNKTE MAGAZIN erforderlichen Nutzungsrechte an den verwendeten Texten und Bildern.

4.4 Der KUNDE verpflichtet sich, den Nutzern eine Möglichkeit zur Bewertung seiner Produkte und des Einkaufserlebnisses zur Verfügung zu stellen bzw. erklärt sich bereit, an einem von PROMEDIA betriebenen Bewertungsportal teilzunehmen.

4.5 Als Teilnehmer des DIENSTES ist der KUNDE zur aktiven Mitwirkung bei der Förderung der Bekanntheit des DIENSTES durch die in Anlage 1 bezeichneten Marketingmaßnahmen verpflichtet.

4.6 Der DIENST darf vom KUNDEN nur im vertraglich festgelegten Umfang und nur unter Beachtung der jeweiligen gesetzlichen Bestimmungen genutzt werden. Der KUNDE verpflichtet sich, keine beleidigenden, verleumderischen, volksverhetzenden, pornografischen, sitten- oder gesetzeswidrigen Inhalte über den DIENST zu verbreiten oder einer solchen Verbreitung, Zugriff oder Bereithaltung durch Dritte zu ermöglichen. Bei Verstößen kann PROMEDIA den KUNDEN von der Nutzung des DIENSTES unmittelbar ausschließen.

4.7 Sofern Anzeichen vorliegen, dass der KUNDE den DIENST nicht im vertraglich festgelegten Umfang nutzt - insbesondere bei Umgehung des Provisionsanspruches - wird der KUNDE Auskunft über die konkrete Nutzung des DIENSTES geben (z.B. durch Einsicht ins Warenwirtschafts-System. Für jeden Fall der nachgewiesenen Umgehung des Provisionsanspruches wird eine Vertragsstrafe i.H.v. 5.000,- €/ eine vom Gläubiger im Einzelfall festzusetzende, im Streitfall vom zuständigen Gericht überprüfbare Vertragsstrafe fällig.

4.8 Der KUNDE verpflichtet sich, PROMEDIA auf erstes Anfordern von sämtlichen Ansprüchen Dritter, die wegen der Verletzung vorstehender Pflichten oder aufgrund sonstiger rechtswidriger Handlungen des Kunden, etwa der Verletzung der Rechte Dritter, insbesondere Urheber-, Datenschutz und Wettbewerbsrechtsverletzungen, gegen PROMEDIA im Rahmen der Nutzung des DIENSTES erhoben werden, freizustellen.

## 5. Provision/Abrechnung

5.1 Für die Nutzung des DIENSTES erhält PROMEDIA eine Provision in Höhe von 10 % des Verkaufspreises jedes über den DIENST verkauften Produktes.

5.2 Die Provisionen werden von PROMEDIA alle 4 Wochen automatisch abgerechnet und per Lastschriftverfahren beim KUNDEN eingezogen. Der KUNDE erteilt PROMEDIA das dafür notwendige SEPA-Lastschriftmandat.

5.3 Bereits abgerechnete Provisionen für stornierte bzw. rückabgewickelte Verkäufe werden dem KUNDEN von PROMEDIA innerhalb von … Wochen nach Meldung des Stornos durch den KUNDEN erstattet. Die Meldung eines Stornos an PROMEDIA muss schriftlich innerhalb von …Wochen/Monaten nach Rückabwicklung des Verkauf erfolgen. Nicht rechtzeitig gemeldete Stornos werden nicht erstattet.

## 6. Aufrechnung / Zurückbehaltung

Eine Aufrechnung oder die Geltendmachung eines Zurückbehaltungsrechtes gegen Ansprüche von PROMEDIA kann nur mit unbestrittenen oder rechtskräftigen festgestellten Ansprüchen oder Ansprüchen, die sich aus demselben Vertragsverhältnis ergeben, ausgeübt werden erfolgen.

## 7. Haftung

7.1 PROMEDIA haftet nur für Schäden, die vorsätzlich oder grob fahrlässig von PROMEDIA, ihren gesetzlichen Vertretern, Mitarbeitern oder Erfüllungsgehilfen verursacht worden sind. Dies gilt nicht für die Haftung für Schäden an Körper oder Verlust des Lebens und bei der Verletzung wesentlicher Vertragspflichten ("Kardinalspflichten").

7.2 Die Haftung bei leicht fahrlässiger Verletzung wesentlicher Vertragspflichten ("Kardinalspflichten") ist begrenzt auf den vorhersehbaren vertragstypischen Schaden. Sie ist für jeden Einzelfall auf höchstens 25.000,00 Euro beschränkt. Sollte ein Schaden über eine Versicherung des Kunden versichert sein oder in der Regel zu versichern sein, ist eine Haftung von PROMEDIA ausgeschlossen.

7.3 PROMEDIA haftet nicht für Inhalte, Informationen und Daten, die vom KUNDEN zur Verfügung gestellt werden.
7.4 Gesetzliche Regelungen, die unabdingbar sind, gelten unbeschadet der vorstehenden Reglungen.

## 8. Vertragsdauer und Kündigung

8.1 Die Mindestvertragslaufzeit beträgt 24 Monate. Der Vertrag verlängert sich jeweils um weitere 12 Monate, wenn er nicht von einer Vertragspartei mit einer Frist von drei Monaten zum Ende der Vertragslaufzeit gekündigt wird. Individuell ausgehandelte abweichende Vereinbarungen über die Mindestvertragslaufzeit, die Dauer einer Vertragsverlängerung oder die Kündigungsfrist haben Vorrang.

8.2 Das Recht zur außerordentlichen Kündigung aus wichtigem Grund, § 314 BGB, bleibt unberührt. Als wichtiger Grund gilt insbesondere, wenn:

8.2 a) der KUNDE schwerwiegend oder trotz Abmahnung wiederholt gegen diese AGB oder sonstige vertragliche Verpflichtungen verstößt;

8.2 b) der KUNDE trotz fristsetzender Mahnung seine Zahlungsverpflichtungen nicht oder nicht vollständig erfüllt;

8.2 c) über das Vermögen des KUNDEN das zumindest vorläufige Insolvenzverfahren eröffnet worden ist.

8.3 Die Kündigung bedarf der Schriftform (E-Mail, Brief, Telefax).

## 9. Vertragsänderungen

9.1 Auf eine Änderung der AGB, Leistungsbeschreibungen und Preistabellen hat die Promediagroup GmbH den Kunden in geeigneter Form, schriftlich oder per E-Mail, hinzuweisen. Ausreichend ist, wenn der Hinweis beinhaltet, in welcher Form und auf welchem Weg der Kunde in zumutbarer Art und Weise die Änderungen zur Kenntnis nehmen kann. Die Änderungen werden im Verhältnis zum Kunden wirksam, wenn er nicht nach Maßgabe der nachstehenden Regelungen der Änderung widerspricht.

9.2 Ab dem Zeitpunkt, zu dem der Hinweis auf die Änderungen erfolgt, hat der Kunde das Recht, binnen einer Frist von 6 Wochen ab Bekanntgabe der Änderungen diesen schriftlich zu widersprechen. Ausreichend ist die rechtzeitige Absendung des Widerspruchs. Erfolgt trotz Hinweis und ausdrücklicher Belehrung kein Widerspruch des Kunden oder widerspricht der Kunde nicht rechtzeitig, so gilt dies als Einverständnis mit der Änderung. Die Änderung tritt dann mit Ablauf der Frist von 6 Wochen in Kraft, es sei denn, in der Änderung selbst wäre ein späterer Zeitpunkt angegeben.

## 10. Datenschutz und Geheimhaltung

10.1 Die Vertragsparteien verpflichten sich, die Speicherung, Verarbeitung und sonstige Nutzung personenbezogener Daten nur im Rahmen der einschlägigen datenschutzrechtlichen Vorschriften vorzunehmen.

10.2 Die Vertragsparteien verpflichten sich zur Geheimhaltung sämtlicher im Rahmen der Zusammenarbeit bekannt gewordenen vertraulichen Informationen der jeweils anderen Vertragspartei. Vertraulich ist insbesondere die jeweilige Preisgestaltung. Den Vertragsparteien steht es frei, gegenüber der jeweils anderen Vertragspartei eine Information ausdrücklich als vertraulich zu benennen.

## 11. Schlussbestimmungen

11.1 Mündliche Nebenabreden wurden nicht getroffen. Änderungen des Auftrages oder sonstiger unter Einbezug dieser AGB´s getroffenen vertraglichen Regelungen bedürfen der Textform . Dies gilt auch für das Abbedingen der Textform.

11.2 Ist der Kunde Kaufmann im Sinne des Handelsgesetzbuches oder juristische Person des öffentlichen Rechts, ist Köln ausschließlicher Erfüllungsort und Gerichtsstand.

11.3 Für alle vertraglichen Beziehungen zwischen den Vertragsparteien gilt ausschließlich deutsches Recht unter Ausschluss des UN-Kaufrechts.

## Marketingmaßnahmen

### ANLAGE 1

Jeder STILPUNKTE-PARTNER-SHOP ist verpflichtet, seine Teilnahme am DIENST deutlich sichtbar zu kennzeichnen, indem er den  „Partner-Shop“-Aufklebers im Eingangsbereich des (im Vertrag bezeichneten) Ladenlokales deutlich sichtbar anbringt;
die Teilnahme am DIENST durch Verlinkung der URL des DIENSTES auf der eigenen Website anzeigt<sup>1</sup>;
in seinem Ladenlokal den DIENST mittels Auslegen von Werbeflyern bewirbt<sup>2</sup>;
bei jedem Verkauf im Ladenlokal einen Werbeflyer für den DIENST in die Einkaufstüte beilegt;

(1) Der KUNDE hat die Möglichkeit eine Verlinkung der eigenen Website auf der Landing-Page des DIENSTES kostenpflichtig dazuzubuchen.

(2) Die Werbeflyer werden den KUNDEN von PROMEDIA kostenlos zur Verfügung gestellt. Der KUNDE hat die Möglichkeit ein Co-Branding auf dem Werbeflyer kostenpflichtig dazuzubuchen.

### 2. Der KUNDE
