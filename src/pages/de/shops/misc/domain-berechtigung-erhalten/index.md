# Berechtigung zur Verwendung von www.stilpunkte.de erhalten

Um Produkte, welche Sie über die STILPUNKTE Shopping-Mall verkaufen bei Facebook und Instagram verlinken zu können, benötigen Sie eine Berechtigung um www.stilpunkte.de verwenden zu dürfen. Diese Berechtigung können Sie kostenlos bei unserem Online-Support beantragen.

## Welche Informationen muss ich Ihnen mitteilen?

Zur Freigabe der Berechtigung benötigen wir Ihre Facebook `Business Manager-ID`. Wählen Sie im [Business Manager](https://business.facebook.com/settings/info) Ihr Unternehmen aus.
