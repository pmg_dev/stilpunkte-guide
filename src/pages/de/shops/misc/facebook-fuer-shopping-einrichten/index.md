---
title: Instagram Shopping
---

Stilpunkte bietet Ihnen die Möglichkeit, Ihre Produkte nicht nur über den Stilpunkte Online Shop zu verkaufen, sondern auch über weitere Vertriebskanäle wie Facebook und Instagram. 
In den nächsten Schritten erklären wir Ihnen, wie Sie Ihren Stilpunkte Online-Shop mit Instagram verbinden.

Um Facebook Shop, also die Produktvorstellung auf Ihrer Facebook-Seite und Instagram Shopping, also das Verlinken einzelner Produkte in Posts einrichten zu können, müssen Sie folgende Schritte beachten und ausführen:

**Schritte**

1. Die Domain stilpunkte.de muss bei der Brand-Safety / Domains als verbundenes Asset gelistet sein
2. Produkt-Katalog im Facebook Commerce Manager anlegen
3. Im Facebook Commerce Manager bei Assets einen Shop hinzufügen

## Commerce Manager

Navigierien Sie zum [Facebook Commerce Manager](https://www.facebook.com/commerce_manager/) und klicken anschließend auf `+ Shop hinzufügen`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-01.jpg)

### Deinen Shop erstellen

Bestätigen Sie, dass Sie einen Shop erstellen möchten in dem sie unten rechts auf `Weiter` klicken.

#### Los geht's

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-02.jpg)

#### Art des Kaufabschlusses auswählen

Wählen Sie `Kaufabschluss auf einer anderen Website` und bestätigen Sie mit dem Klick auf `Weiter`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-03.jpg)

#### Vertriebskanäle auswählen

Sie können nun die Vertriebskanäle auswählen, über die Sie verkaufen möchten.
Klicken Sie den jeweiligen Kanal an und bestätigen Sie mit einem Klick auf `Weiter`. 

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-04.jpg)

#### Produkte hinzufügen

Um Produkte zu Ihrem Shop hinzuzufügen müssen Sie zuerst einen Katalog erstellen. Klicken Sie auf `+` und geben Ihrem Katalog einen Namen. 
Bestätigen Sie mit einem Klick auf `Weiter`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-05.jpg)

#### Wo verkaufst du deine Produkte?

Sie müssen nun Ihren Shop mit Facebbok verlinken. Geben Sie in dem Pop-Up Fenster die URL des Online-Shops per `CopyPaste` ein und bestätigen Sie mit einem Klick auf `Weiter`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-06.jpg)

#### Zusammenfassung

Schließen Sie jetzt die Einrichtung Ihres Instagram Online Shops ab. Sie sehen hier eine Zusammenfassung mit den wichtigsten Details wie Art des Kaufabschlusses, der Vertriebskanäle und der Produkte. Bestätigen Sie hier, dass Facebook Ihren Shop überprüfen darf und Sie den Verkäufervereinbarungen zustimmen. 
Bestätigen Sie mit einem Klick auf `Einrichtung abschließen`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-07.jpg)

Da Sie im Vorfeld schon einen Katalog erstellt haben, können Sie nun Artikel hinzufügen. Klicken Sie im Pop-Up Fenster auf `Artikel hinzufügen`. Sie werden zu dem Bereich `Datenquellen`weitergeleitet. Lassen Sie sich nicht von dem Begriff Datenquelle irritieren. Facebook bietet Ihnen nicht nur die Möglichkeit, ihre Produkte einzeln einzupflegen sondern auch eine Datei mit allen Artikelinformationen hochzuladen. Wie Sie diese Datei erstellen, erfahren Sie weiter unten.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-08.jpg)

## Datenquellen

Um eine neue Datenquelle hinzuzufügen, klicken Sie nun auf `Artikel hinzufügen`. 

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-10.jpg)

### Artikel zu deinem Katalog hinzufügen

Sie können jetzt Artikel zu Ihrem Katalog hinzufügen. Wählen Sie dazu `Bulk-Upload nutzen`und klicken Sie auf `Weiter`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-11.jpg)

### Bulk-Upload nutzen

#### Upload-Optionen auswählen

Wählen Sie als Upload-Option `Geplanter Feed`. 

Klicken Sie jetzt auf `Weiter`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-12.jpg)

#### Feed einrichten

Bevor Sie hier weitermachen können müssen Sie sich in Ihrem Stilpunkte Online-Shop in den **Eintellungen** unter **Marketing** eine CSV Datei Ihrer Produkte anfertigen lassen.

Kopieren Sie die URL und fügen Sie sie hier ein und bestätigen mit einem Klick auf `Weiter`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-13.jpg)

#### Aktualisierungszeitplan

Bestimmen Sie nun, wie häufig Facebook bzw. Instagram den Datenfeed aktualisieren soll. Eine regelmäßige Aktualisierung ist wichtig um die Bestandsinformationen in Ihrem Katalog auf dem neuesten Stand zu halten.

Optimal ist eine tägliche Aktualisierung, wählen Sie `Täglich` aus und klicken Sie auf `Weiter`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-14.jpg)

#### Einstellungen

Geben Sie jetzt Ihrer Datenquelle einen eindeutigen Namen, damit Sie sie immer wieder identifizieren können und klicken Sie auf `Hochladen`.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-15.jpg)

#### Erster Upload

Nun werden Ihre Produkte hochgeladen. Ihnen wird der genaue Fortschritt des Uploads angezeigt. Sie brauchen nicht das Ende des Uploads abzuwarten, der Upload wird auch nach Schließen der Webseite weiter ausgeführt.

![](/images/shops/facebook-fuer-shopping-einrichten/instagram-shopping-16.jpg)

---

## Links

- [Facebook Manager](https://www.facebook.com/commerce_manager/)
- [Einen Datenfeed für einen Katalog aktualisieren](https://www.facebook.com/business/help/1013894008673621)
- [Artikel mit einer Datenfeed-Datei in einen Katalog hochladen](https://www.facebook.com/business/help/125074381480892?id=725943027795860)
- [Supported Fields for Products - Dynamic Ads & Commerce](https://developers.facebook.com/docs/marketing-api/catalog/reference/#da-commerce)
