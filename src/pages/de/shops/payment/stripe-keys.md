---
title: Wie bekomme ich den Stripe "Publishable Key" und "Secret Key"
---

Folgen Sie den unten genannten Schritten, um Live Publishable Key, Live Secret Key von Stripe zu erhalten

**Schritt 1:** Gehen Sie zu [stripe.com](https://www.stripe.com/) und klicken Sie auf Anmelden.

![](/images/shops/payment/stripe-screenshot-01.jpg)

**Schritt 2:** wenn Sie ein Stripe-Konto haben, melden Sie sich an.

![](/images/shops/payment/stripe-screenshot-02.jpg)

**Schritt 3:** Klicken Sie links im Menü auf Entwickler.

![](/images/shops/payment/stripe-screenshot-03.jpg)

**Schritt 4:** Das Dropdown-Menü Entwickler öffnet sich auf der linken Seite des Menüs. Klicken Sie auf API-Schlüssel.

![](/images/shops/payment/stripe-screenshot-04.jpg)

**Schritt 5:** Klicken Sie auf den "Live-Schlüsseltoken einblenden" Button.

![](/images/shops/payment/stripe-screenshot-05.jpg)

**Schritt 6:** Der Veröffentlichbarer Schlüssel und Geheimschlüssel werden angezeigt. Kopieren Sie diese Schlüssel.

**Schritt 7:** Fügen Sie nun die zuvor kopierten Zugangsschlüssel in die STILPUNKTE® Verwaltung.
