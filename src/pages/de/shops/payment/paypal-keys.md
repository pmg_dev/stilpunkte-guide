---
title: Wie bekomme ich die Paypal Client ID und den Secret Key?
---

Um die Client ID und den Secret Key von Paypal zu bekommen benötigst du ein Paypal Konto.

**Schritt 1:** Öffne folgende Webseite [developer.paypal.com](https://developer.paypal.com) und klicke auf `Log into Dashboard`

![](/images/shops/payment/paypal-screenshot-11.jpg)

## Paypal App erstellen

**Schritt 2:** Klicke links im Menu auf `My Apps and Credentials`. Lege den Schalter oben von `Sandbox` um auf `Live`.

![](/images/shops/payment/paypal-screenshot-12.jpg)

**Schritt 3:** Anschließend klicke auf `Create App` und gebe deiner neuen "App" einen Namen, hier bietet sich `Stilpunkte Shopping-Mall` an, du kannst aber jeden Namen wählen, den du möchtest. Nachdem du fertig bist, klicke auf `Create App`

![](/images/shops/payment/paypal-screenshot-13.jpg)

## Paypal App Client ID und Secret Key

**Schritt 4:** Sobald die App erstellt ist, öffnen sich die Details der eben erstellten App. Du solltest nun die Client ID sehen und darunter eine Option (Show) um den Secret Key zu sehen.

![](/images/shops/payment/paypal-screenshot-14.jpg)

**Schritt 5:** Klicke nun auf diesen Link und lasse dir den Secret Key anzeigen. In der oberen rechten Seite siehst du einen Schalter, mit welchem du zwischen Sandbox (Testzahlungen) und Live (echte Zahlungen) hin und her schalten kannst. Beachte, dass Sandbox und Live unterschiedliche Client IDs und Secret Keys haben.

**Schritt 6:** Gebe die Keys in die Oberfläche bei STILPUNKTE® ein.

![](/images/shops/payment/paypal-screenshot-15.jpg)
