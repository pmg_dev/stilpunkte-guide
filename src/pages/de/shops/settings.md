---
title: Einstellungen
description: Erfahren Sie, wie Sie Ihren Online-Shop richtig konfigurieren und auf Ihre Bedürfnisse und Möglichkeiten einstellen können.
meta:
  - name: keywords
    content: Einstellungen, Farben, Logos, Rechnungsinformationen, Zahlungseinstellungen, Versand, Shopping-Mall, Stilpunkte
  - name: og:title
    content: Einstellungen Ihres Online-Shops
---

Dieser Bereich bietet die Möglichkeit individuelle gestalterische Veränderungen und Einstellungen an Ihrem Shop vorzunehmen und diesen nach Fertigstellung auch zu aktivieren. Klicken Sie einfach in das entsprechende Kästchen. {% .lead %}

## Übersicht

Sie sehen hier den Kasten zur Aktivierung Ihres Online-Shops mit dem Link `Allgemeine Geschäftsbedingungen`.
Darunter sind die Kästen mit den jeweiligen Einstellmöglichkeiten Ihre Online-Shops.
Mit einem Klick in eines der Kästchen öffnet sich darunter die Einstellmaske.

## Aktivierung des Online-Shops

Sie haben hier die Möglichkeit Ihren Online-Shop zu aktivieren.

Solange Ihr Shop inaktiv ist, ist er nur für Sie sichtbar. Sie können alle individuellen Anpassungen vornehmen, bis der Shop ganz Ihren Vorstellungen entspricht.
Sobald Sie Ihren Shop aktivieren, ist dieser online. Anschließend ist Ihr Online-Shop in der STILPUNKTE® Shopping Mall für alle User sichtbar.

{% callout type="warning" title="Hinweis" %}
Sie können auch im aktiven Zustand Änderungen vornehmen, bedenken Sie nur, dass diese sofort für alle sichtbar sind.
{% /callout %}

Der Link `Allgemeine Geschäftsbedingungen` bringt Sie zu dem aktuellsten Stand der allgemeinen Geschäftsbedingungen. Sie können diese jederzeit einsehen und der Kopfzeile das Datum der letzten Aktualisierung entnehmen.

{% callout type="danger" title="Bitte beachten!" %}
Wenn Sie Ihren Shop aktivieren, akzeptieren Sie die allgemeinen Geschäftsbedingungen.
{% /callout %}


## Farben

**Ändern Sie die Farben Ihres Online-Shops**

Sie können die Farben des Menüs, des Hintergrundes und des Footers ändern. Beim Menü und beim Footer können Sie auch die Textfarbe verändern.

#### Schritte

1. Klicken Sie dafür mit der Maus in das entsprechende Kästchen.

![](/images/shops//settings-color-1.jpg)

2. Ein neues Fenster erscheint in dem Sie entweder die richtige Farbe in der Farbskala anwählen können oder Sie tragen die RGB Werte oder die Hex-Werte ein.

![](/images/shops//settings-color-2.jpg)

3. Klicken Sie mit der Maus in einen Bereich außerhalb des Einstellungsfensters um dieses Fenster zu schließen.
4. Ändern Sie nach Ihren Wünschen die Farben Ihres Online-Shops.
5. Klicken Sie auf `Speichern` um die Änderungen zu speichern.

## Logos

**Laden Sie das Logo für Ihren Shop hoch**

Sie können Logos für Ihr Menü, den Footer oder die Rechnung hochladen.

#### Schritte

1. Klicken Sie auf den Button `Auswählen` für den jeweiligen Bereich.
1. Sie können jetzt die Logo-Datei von Ihrem Rechner auswählen.
1. Klicken Sie auf `Öffnen`.
1. Speichern Sie Ihre Änderungen mit einem Klick auf `Speichern`.

Möchten Sie ein bereits hochgeladenes Logo wieder löschen, klicken Sie bei dem jeweiligen Logo auf den Button `Löschen`.

## Rechnungsinformationen

**Verwaltung aller rechtlichen Informationen für Ihren Shop**

In diesem Formular müssen Sie alle rechtlich relevanten Angaben für Ihre Rechnungen eintragen. Die Informationen teilen sich auf in Unternehmensanschrift und Rechnungsinformationen. Alle notwendigen Angaben sind mit Sternen markiert. Tragen Sie alle Informationen ein und klicken Sie auf `Speichern`. Nicht ausgefüllte Zeilen erscheinen später nicht auf Ihrer Rechnung.

{% callout type="danger" title="Bitte beachten!" %}
  Bitte überprüfen Sie alle Ihre Angaben auf Richtigkeit und Vollständigkeit.
{% /callout %}

## Zahlungseinstellungen

**Welche Zahlungsmethoden Kunden angeboten werden sollen**

Der STILPUNKTE® Online-Shop bietet Ihnen und Ihren Kunden Paypal und Stripe als Zahlungsmöglichkeit an.
Paypal und Stripe müssen von Ihnen eingerichtet werden. Die genauen Beschreibungen dazu finden Sie unten.

{% callout type="danger" title="Bitte beachten!" %}
  Sie müssen sowohl Paypal als auch Stripe aktivieren.
{% /callout %}

### Paypal

Paypal ist ein Betreiber eines Online-Bezahldienstes, der zur Begleichung von Mittel- und Kleinbeträgen zum Beispiel beim Ein- und Verkauf im Online-Handel genutzt werden kann.

Wie Sie an die notwendigen Keys gelangen, beschreiben wir in unserer [ausführlichen Anleitung](/de/shops/payment/paypal-keys/).

### Stripe (Kreditkartenzahlung)

Stripe ist ein Online-Bezahldienst mit Sitz im kalifornischen San Francisco. Die Dienstleistungen des Unternehmens werden in 25 Ländern angeboten. Stripe verwendet ein selbstlernendes Betrugs-Präventions-System.

Stripe akzeptiert folgende Kreditkarten: MasterCard, Visa, American Express, Diners Club, Apple Pay und Google Pay.

Wie sie an die notwendigen Keys gelangen, beschreiben wir in unserer [ausführlichen Anleitung](/de/shops/payment/stripe-keys/).

## Versand & Zustellung

**Welche Versandoptionen Kunden angeboten werden sollen**

Sie können hier Ihre Versandoptionen aktivieren. Zur Auswahl stehen:

- Deutschland
- EU
- weltweit

Sie haben die Möglichkeit Ihre Versandoptionen auszuwählen und die Versandkosten einzutragen. Die Versandkosten werden hier in Volumen angegeben. So können Sie anhand Ihres Versandunternehmens eine Preisstaffelung erstellen.

#### Schritte

1. Setzen Sie einen Hacken in das Kästchen `Versand nach ... für Kunden erlauben`.
1. Klicken Sie auf den Button `Hinzufügen`.
1. Tragen Sie in die neue Maske das Volumen und die Kosten ein.
1. Durch erneutes Klicken auf `Hinzufügen` können Sie eine neue Maske erstellen. So können Sie Ihre Versandkostenstaffelung genau eintragen.
1. Bei mehreren Einträgen können Sie die Reihenfolge ändern, indem Sie rechts auf den jeweiligen Doppelpfeil klicken, gedrückt halten und an die richtige Position ziehen.
1. Löschen Sie eine Versandkostenoption durch Klick auf den kleinen `Papierkorb` rechts.
1. Klicken Sie unten auf `Speichern`.

### Warenbestand

#### Verkauf bei niedrigem Bestand

Sie haben die Möglichkeit Waren zu verkaufen, obwohl die Waren laut Warenbestand nicht verfügbar ist. Diese Einstellung ist hilfreich, falls Sie keinen Warenbestand im System angeben möchten.

Sollten Sie diese Einstellung deaktivieren, wird der Verkauf von Produkten mit einem Warenbestand von kleiner-gleich Null Einheiten gestoppt.

Diese Einstellung ist standardmäßig aktiviert.

#### E-Mail Update

Sie haben die Möglichkeit benachrichtigt zu werden, falls Ihr Warenbestand pro Produkt auf niedriger als zehn Einheiten sinkt.

Diese Einstellung ist standardmäßig deaktiviert.

### Lokale Abholung

Sie können in den Versandoptionen die lokale Abholung anbieten. Der Kunde kauft und bezahlt seine Bestellung online und holt diese in der angegeben Adresse selbst ab.

Unter `Einstellungen → Versand & Zustellung → Lokale Abholung` können Sie diese Option aktiveren und eine Abhol-Adresse eintragen.

Falls Sie Kosten für die lokale Abholung angeben möchten, können Sie den Betrag unter der Einstellung `Kosten` eintragen. Möchten Sie keine Kosten angeben, tragen Sie hier `0` ein. Kosten für die lokale Abholung werden in der Rechnung unter `Lieferkosten` aufgeführt.

## Marketing

### Instagram Shopping

[Einrichtung bei Facebook](/de/shops/misc/facebook-fuer-shopping-einrichten/)

## Bedingungen

- Alle Pflichtfelder sind mit einem `*` versehen.
- Mit Aktivierung des Online-Shops akzeptieren Sie unsere allgemeinen Geschäftsbedingungen.
- Nicht ausgefüllte Felder erscheinen später nicht auf Ihrer Rechnung.
- Überprüfen Sie alle Angaben auf Ihre Richtigkeit.
