---
title: Produkte
meta:
  - name: description
    content: Erfahren Sie, wie Sie Produkte zu Ihrem Online-Shop hinzufügen und bearbeiten können, welche Informationen sie hinterlegen können und sollten und wie Sie Produkt-Fotos hochladen können.
  - name: og:title
    content: Produkte in Ihrem Online-Shop
  - name: og:description
    content: Erfahren Sie, wie Sie Produkte zu Ihrem Online-Shop hinzufügen und bearbeiten können, welche Informationen sie hinterlegen können und sollten und wie Sie Produkt-Fotos hochladen können.
---

Auf dieser Seite haben Sie die Möglichkeit neue Produkte anzulegen und erhalten eine Übersicht über Ihre bereits eingetragenen Produkte. {% .lead %}

{% callout title="Tipp!" %}
Über den Button `zum Online-Shop` gelangen Sie direkt auf Ihren STILPUNKTE® Online-Shop.
{% /callout %}

## Übersicht

Sobald Sie Produkte eingetragen haben, erscheint hier eine Übersicht mit allen Produkten und den wichtigsten Infos:

- ein Bild des Produktes
- aktiv (ist es im Shop sichtbar)
- kaufbar
- Artikel Nr.
- Titel (Name des Produktes)
- Preis

## Ein neues Produkt anlegen

#### Schritte

1. Klicken Sie auf den Button `+ Produkt anlegen`
1. `Sichtbar` entscheiden Sie, ob das Produkt nur im Shop, nur im Product Guide oder im Shop und gleichzeitig im Product Guide angezeigt werden soll.
1. `Kaufbar` tragen Sie ein, ob das Produkt gerade verfügbar ist.
1. Geben Sie alle produktrelevanten Daten ein.
1. Geben Sie Ihrem Produkt eine Beschreibung und tragen Sie weitere wichtige Hinweise ein.
1. Fügen Sie Produktdetails hinzu.
1. Klicken Sie dafür auf den Button `+ Hinzufügen`.
1. Geben Sie die Informationen ein.
1. Sie können die Produktdetails untereinander verschieben. Klicken Sie dafür rechts auf das Icon `Doppelpfeil`.
1. Mit gedrückter linker Maustaste können Sie das Produktdetail an die richtige Stelle ziehen.
1. Löschen Sie ein Produktdetail mit einem Klick auf das Icon `Papierkorb` rechts neben dem Produktdetail.
1. Ordnen Sie Ihrem Produkt Kategorien zu.
1. Tragen Sie die Produktmaße ein.
1. Geben Sie ihrem Produkt Keywords und eine Meta Beschreibung.
1. Laden Sie Produktfotos hoch. Für diesen Schritt müssen Sie Ihre Änderungen erst `Speichern & Schließen` und das Produkt neu öffnen. Jetzt steht Ihnen für diesen Schritt ein gestricheltes Feld zur Verfügung.
1. Ziehen Sie jetzt entweder ein Bild per Drag & Drop von Ihrem Desktop in das Feld oder klicken Sie mit der Maus in das Feld und wählen ein Foto von Ihrem Computer aus.
1. Wollen Sie die Reihenfolge der Fotos ändern, wählen Sie ein Foto mit gedrückter linker Maustaste an die richtige Stelle.
1. Möchten Sie ein Foto löschen, ziehen Sie es mit gedrückter linker Maustaste in das Feld `Foto zum Löschen hier ablegen`.
1. Sollte es ein saisonales Produkt sein, haben Sie zum Schluss die Möglichkeit eine begrenzte Laufzeit für das Produkt einzutragen. Das Produkt wird dann nur während der Laufzeit angezeigt, auch wenn es auf `sichtbar` gestellt ist.
1. Klicken Sie auf `Speichern & Schließen`.


## Anlegen / Bearbeiten

Sie können neue Produkte über den Button `+ Produkt anlegen` erstellen, anschließend öffnet sich eine Maske bei der Sie alle wichtigen Informationen über Ihre Produkte eintragen können. Sie bearbeiten bereits angelegte Produkte über das `Schraubenschlüssel`-Symbol.

### Produkt im Online-Shop anzeigen

`Dieses Produkt dem Kunden anzeigen` gibt Ihnen die Möglichkeit ein Produkt anzulegen, es aber noch nicht im Shop anzeigen zu lassen.

`Dieses Produkt kann gekauft werden` informiert, ob das Produkt vorrätig ist oder nicht.

Sie können hier auch entscheiden, wo Ihr Produkt angezeigt werden soll.
Zur Auswahl stehen:

- 'Im Shop & und im Product Guide anzeigen'
- 'Nur im Product Guide anzeigen'
- 'Nur im Shop anzeigen'

### Produktinformationen

Geben Sie dem Kunden so viele Informationen wie möglich. Beachten Sie jederzeit, dass dem Kunden ausschließlich diese Informationen für einen Kaufentscheid zur Verfügung stehen.
Sie können alle relevanten Informationen wie Titel, Marke, Produktbezeichnung, Artikelnummer, Steuersatz, Bruttopreis, wichtige Hinweise, etc. eintragen.

{% callout title="Tipp!" %}
Ein gut beschriebenes Produkt kann die Rate von Retouren verbessern.
{% /callout %}

### Warenbestand

Sie können den Warenbestand Ihrer Produkte angeben. Der Warenbestand wird automatisch bei jeder Bestellung verringert und aktualisiert. Sollten Sie Bestellungen stornieren, wird die stornierte Ware automatisch dem Warenbestand zugeordnet.

Sie können den Verkauf von Produkten stoppen, sobald die Waren nicht mehr verfügbar sind. Weitere Informationen erhalten Sie in den [Einstellungen → Versand](/de/shops/settings.html#warenbestand).

### Beschreibung

Beschreiben Sie Ihr Produkt in der Produktbeschreibung ausführlich. Je näher Sie an den Richtwert von 400 Zeichen kommen, desto besser ist das für Ihr SEO. Vermeiden Sie Texte einfach zu kopieren, da Google dies negativ auffast. Auch eine zu kurze Produktbeschreibung kann sich negativ auf Ihr SEO auswirken und zu geringen Kaufabschlüssen führen.

#### weitere wichtige Hinweise

Wichtige Hinweise werden auf der Produktseite unterhalb des `In den Warenkorb`-Buttons aufgeführt.

![](/images/shops//products-wichtige-hinweise.jpg)

{% callout title="Tipp!" %}
Geben Sie einen Hinweis pro Zeile ein.
{% /callout %}

### Produktdetails

Um dem Kunden weitere Details zu Ihrem Produkt zur Verfügung zu stellen, eignen sich die Produktdetails. Diese erscheinen auf der Produktseite als Tabelle neben der Produktbeschreibung und bestehen aus einer Überschrift (links) und einem Inhaltsfeld (rechts).

### Kategorien

Ihre Produkte erscheinen in Ihrem STILPUNKTE® Online-Shop und auch im Product Guide des STILPUNKTE® Online-Portals. Die `Kategorie 1` ist zur Zuordnung im STILPUNKTE® Product Guide, alle weiteren Kategorien dienen zur Katalogisierung in Ihrem Online-Shop.

{% callout type="warning" title="Hinweis" %}
Beachten Sie, dass die `Kategorie 1` Ihr Produkt in den STILPUNKTE® Product Guide einsortiert und die Kategorien `2` und `3` für den Online-Shop gelten. Sie können selber keine Kategorien erstellen.
Die Kategorien bilden im Online-Shop nachher Ihr Menü, deswegen ist es sehr wichtig, dass Sie die Produkte so gut wie möglich kategorisieren. Kategorie `2` bildet dabei das Menü und Kategorie `3` das Untermenü in Ihrem Online-Shop.
{% /callout %}

### Verpackung

Die Verpackungsinformationen werden für die Versandkostenberechnung benötigt und erscheinen nicht in Ihrem Online-Shop. Angaben zur Höhe, Breite, Tiefe und Gewicht sind Pflichtangaben.

### Keywords

Beschreiben Sie Ihr Produkt in kurzen Schlagworten. Diese werden in der internen Suche berücksichtigt und kann Ihr Suchmaschinen Ranking verbessern. Die Vorschläge von ähnlichen Produkten auf der Produktseite basiert auch auf den Keywords.

Trennen Sie jedes Keyword mit einem `,` um ein neues Keyword einzugeben.

{% callout type="warning" title="Hinweis" %}
Bei den Keywords geht es darum, Ihr Produkt so gut es geht zu beschreiben und zu definieren. Fächern Sie die Begriffe zu weit, z.B. Ihr Produkt ist ein Waschbecken und Sie ergänzen neben dem Keyword `Badezimmer-Waschbecken` auch ein Keyword `Design-Heizkörper`, geht das eventuell zu Kosten der Kundenerfahrung. Sucht er einen bestimmten Artikel und bekommt aber auch gänzlich andere Artikel angezeigt, kann es sein, dass der Kunde das Gefühl hat, die Suche würde nicht ordentlich arbeiten.
{% /callout %}

### Fotos

Beim Online-Shopping spielen Fotos eine besonders wichtige Rolle.
Da der Kunde nicht in das Ladenlokal kommen kann um sich das Produkt aktiv anzusehen und anzufassen, ist die Qualität der Fotos besonders wichtig. Sie ersetzen das haptische Einkaufserlebnis des Kunden. 
Achten Sie deshalb darauf, dass Ihre Fotos das Produkt getreu wiedergeben. D.h. Details , Material und Farbe sollten sehr gut erkennbar sein. In Kombination mit einer guten Produktbeschreibung kann das über Verkauf oder Nicht-Verkauf entscheiden.

Es können max. 5 Fotos hochgeladen werden.

Die optimale Größe jedes Fotos ist: `1080 px x 880 px`. Quadratische oder Hochformat-Fotos sind nicht möglich, da diese Fotos sowohl im STILPUNKTE® Product Guide und in der STILPUNKTE® Shopping Mall genutzt werden.
Damit diese Seiten in einem einheitlichen Design erscheinen, sollten es bitte nur querformatige Bilder in der oben genannten Größe sein.

### Produkt löschen

Sie haben natürliche auch die Möglichkeit ein Produkt komplett aus Ihrem Online-Shop zu löschen.

#### Schritte

1. Klicken Sie links in der Menübar auf `Produkte`.
1. Sie sind jetzt in Ihrer Produktübersicht.
1. Suchen Sie nun das zu löschende Produkt raus und klicken Sie rechts bei diesem Produkt auf das Icon `Papierkorb`.
1. Sie müssen jetzt noch einmal bestätigen, ob Sie dieses Produkt wirklich löschen möchten. Dies ist eine reine Sicherheitsmaßnahme, damit Sie nicht aus Versehen etwas falsches löschen.
1. Das Produkt ist nun komplett mit allen Angaben aus Ihrem Online-Shop gelöscht.

## Produkt-Varianten

Sie haben die Möglichkeit ein Produkt in Varianten in Ihrem Shop anzulegen.

**Beispiel:** `Pullover A` gibt es in diversen Farben und/oder Größen.

Legen Sie hierfür erst das Hauptprodukt komplett an, speichern und schließen Sie es. Sie befinden Sich jetzt wieder auf der Produktübersicht. Klicken Sie bei dem zu bearbeitenden Produkt rechts auf das Icon `+`. Sie gelangen zu einer neuen Eingabemaske. Füllen Sie hier die Felder wie bei den anderen Produkten aus und klicken Sie auf `Speichern & Schließen`. In der Übersicht wird Ihnen jetzt Ihr Hauptprodukt angezeigt und mit einem eingerückten Pfeil darunter die Produkt-Varianten.

Die Übersichtszeile des Hauptproduktes hat sich auch etwas verändert. Der Warenbestand und die Kaufbarkeit werden hier nicht mehr angezeigt und falls Ihre Produkt-Varianten verschiedene Preise haben, wird Ihnen ein ab-Preis angezeigt. Sie können dem Hauptprodukt jederzeit weitere Varianten mit einem Klick auf das Icon `+` hinzufügen. Darunter erscheinen die Produkt-Varianten. Hier sehen Sie jetzt auch die Kaufbarkeit, den Warenbestand und den Preis. Die Produktvarianten werden dem Kunden auf der Produktseite unter dem Preis angezeigt und verlinkt.

## Bedingungen

- Alle Pflichtfelder sind mit einem `*` versehen.
- Achten Sie auf eine gute Zuteilung der Kategorien.
- Denken Sie daran den Mehrwertsteuersatz an Ihr Produkt anzupassen.
- Geben Sie dem Produkt eine aussagekräftige META Beschreibung und Keywords und versuchen Sie, sich an die vorgegeben Länge der Zeichen zu halten.
- Halten Sie sich an die vorgegebene optimale Größe der Fotos.

{% callout type="danger" title="Bitte beachten!" %}
  Es ist nicht möglich eine Dienstleistung direkt zu verkaufen, da diese anders besteuert werden. Sie können aber einen Gutschein für eine Dienstleistung verkaufen. 

  Beispiel: Sie können keine Spa-Behandlung über den Online-Shop verkaufen, aber einen (Online-) Gutschein für eine Spa-Behandlung kann angeboten werden.
{% /callout %}
