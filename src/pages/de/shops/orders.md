---
title: Bestellungen
description: Erfahren Sie, wie Sie Bestellungen in Ihrem Online-Shop einsehen können und für den Versand vorbereiten.
meta:
  - name: keywords
    content: Stilpunkte, Shopping-Mall, Bestellungen, Einsehen
  - name: og:title
    content: Bestellungen
---

Die Bestellabwicklung erfolgt in der Verwaltung Ihres Online-Shops.

## Übersicht

Angezeigt wird hier jede Bestellung mit den wichtigsten Informationen.
Von links nach rechts stehen Ihnen hier folgende Informationen zur Verfügung:

- Bestellnummer
- Bestelldatum
- Name des Kunden
- Betrag
- vom Kunden gewählte Bezahlform
- Status der Bestellung (Ausstehend-Storniert-Rückerstattung-Rückerstattet-Abholung-Versand-Bezahlt)
- Provisionsindikator - das kleine Icon zeigt Ihnen, dass die Bestellung kommissioniert ist und über die STILPUNKTE GmbH verrechnet wird
- das Icon `drei Punkte` bringt Sie zu der jeweiligen Bestellung

## Bestellung ansehen

Klicken Sie in der Übersicht bei einer Bestellung auf das Icon `drei Punkte` und erhalten Sie detaillierte Informationen zu einer Bestellung.

Im Header der Seite wird Ihnen die Bestellnummer angezeigt und der Bezahlstatus der Bestellung.

Mit den Buttons `Rechnung`, `Lieferschein` und `Rücksendeformular` können Sie die jeweiligen Dokumente als PDF öffnen und ausdrucken.

Darunter werden Ihnen getrennt voneinander die Liefer- und die Rechnungsadresse angezeigt und eine Auflistung der Bestellung mit Bestellnummer und der bestellten Artikel.

Sie können hier noch verschiedene Aktionen ausführen:

- `Paket wurde verschickt` markieren Sie die Bestellung als verschickt
- `Zurückerstatten` veranlassen Sie eine Rückerstattung
- `Stornieren` stornieren Sie die Bestellung


## Paket verschicken

Sobald eine Bestellung eingeht, werden Sie per Mail informiert. Sie können nun die Bestellung zum Versand mit Ihrem Versanddienstleister fertig machen und in Ihrem Shop mit `Paket wurde verschickt` markieren.
Der Online-Shop bietet ihnen die Möglichkeit Rechnung, Lieferschein und Rücksendeformular direkt auszudrucken.
Legen Sie Rechnung und Rücksendeformular der Bestellung bei und kleben Sie den Lieferschein außen an das Paket.

{% callout type="warning" title="Hinweis" %}
  Die generierte eMail die Sie bei jeder eingehenden Bestellung erhalten ist eher schmucklos und besteht nur aus Text. So wird verhindert, dass das Mail-Programm Sie für einen Newsletter hält und als Spam einordnet.
{% /callout %}

## Rückerstattungen

Sollte der Kunde einen oder mehrere Artikel seiner Bestellung mit Hilfe des Rücksendeformulars zurückschicken, haben Sie hier die Möglichkeit dies zu veranlassen.

#### Schritte

1. Klicken Sie auf den Button `Zurückerstatten`.
1. Es öffnet sich ein weiteres Feld mit allen Einzelheiten der Bestellung.
1. Markieren Sie die zu erstattenden Artikel und deren Anzahl.
1. Klicken Sie auf den Button `Erstatten`.
1. Der Kunde und Sie erhalten automatisch eine aktualisierte Rechnung.
1. Wickeln Sie mit dem Kunden die Rücksendung der Artikel ab.
1. Erstatten Sie dem Kunden den schon gezahlten Betrag.

{% callout type="warning" title="Hinweis" %}
  Bei bereits gezahlter Umsatzbeteiligung an die STILPUNKTE GmbH, wird Ihnen der Betrag wieder gutgeschrieben. Tragen Sie die Rückerstattung ganz einfach in Ihrer Bestell-Übersicht ein.
{% /callout %}

## Stornierung

Sie können eine Bestellung jederzeit stornieren.

#### Schritte

1. Klicken Sie auf den Button `Stornieren`.
1. Sie erhalten jetzt automatisch ein neues Dokument: **Stornorechnung**. Dieses erhalten Sie und der Kunde automatisch per Mail.
1. Stoppen Sie den Versand der Artikel.
1. Sollte der Kunde schon gezahlt haben, erstatten Sie ihm den Betrag zurück.

{% callout type="warning" title="Hinweis" %}
  Sie müssen die Bestellung auch bei Ihrem Zahlungsdienstleister stornieren, damit der Kunde sein Geld zurückerhält.
{% /callout %}

{% callout type="danger" title="Bitte beachten!" %}
  Eine Stornierung kann nicht widerrufen werden!
{% /callout %}

## Rückzahlung 

Damit Ihre Kunden Ihr Geld zurückerhalten, müssen Sie die Zahlung bei Ihrem Zahlungsdienstleister stornieren.

#### Zahlungsart PayPal

So erstatten Sie eine Zahlung via PayPal zurück:

1. Loggen Sie sich in Ihrem Paypal Account ein.
1. Gehen Sie zu Ihren `Aktivitäten`.
1. Wählen Sie die Zahlung aus, die Sie zurückerstatten möchtest, und klicken Sie auf `Rückzahlung senden`.
1. Geben Sie den Betrag ein, den Sie senden möchten.
1. Klicken Sie auf `Weiter`.
1. Überprüfen Sie Ihre Angaben und klicken Sie dann auf `Rückzahlung senden`.

Bei PayPal Sie können innerhalb von 180 Tagen nach dem Datum der ursprünglichen Zahlung eine vollständige oder Teilrückzahlung vornehmen. Sobald Sie eine Rückzahlung gesendet haben, können Sie sie nicht mehr stornieren.

Bei Zahlungen, die mit einem Gutschein oder Geschenkgutschein getätigt werden, können Sie nur eine vollständige Rückerstattung vornehmen. Wenn Sie eine Teilrückzahlung senden möchten, gehen Sie zu `Geld senden` und erstellen Sie eine neue Zahlung. Verweisen Sie in den Anmerkungen auf die ursprüngliche Zahlung und senden Sie diese als persönliche Zahlung.

Sie zahlen bei PayPal für die Rückerstattung einer Zahlung für Waren oder Dienstleistungen keine Gebühren, aber PayPal erstattet Ihnen nicht die Gebühren, die Sie ursprünglich als Verkäufer gezahlt haben.

Um weitere Informationen zu Gebühren zu erhalten, klicken Sie bei PayPal unten auf `Gebühren`.

## Dokumente

Folgende Dokumente stehen Ihnen zum Ausdrucken zur Verfügung:

- Rechnung
- Lieferschein
- Rücksendeformular

Sollten Sie Rückerstattungen oder Stornierungen vorgenommen haben, stehen Ihnen auch noch folgende Dokumente zur Verfügung:

- Rechnungskorrekturen
- Stornorechnung

## Bedingungen

- Alle Pflichtfelder sind mit einem `*` versehen.
- Denken Sie daran, eine Stornierung kann nicht rückgängig gemacht werden.
- Legen Sie jeder Bestellung die Rechnung, das Rücksendeformular und den Lieferschein bei.
- Informieren Sie bei einer Rückerstattung oder Stornierung auch Ihren Zahlungsdienstleister, damit der Kunde sein Geld zurück bekommt.
