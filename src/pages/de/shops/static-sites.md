---
title: Statische Seiten
description: 
meta:
  - name: keywords
    content:
  - name: og:title
    content: Statische Seiten Ihres Online-Shops
---

Auf den Seiten können Sie alle rechtlichen Texte und AGB einstellen. Bei Aktivierung erscheinen diese Seiten im Footer. {% .lead %}

## Übersicht

Sie bekommen hier eine Übersicht über alle Seiten. Die Reihenfolge und Anzahl der Seiten sind vorgegeben. Dieser Bereich wird aber kontinuierlich weiter ausgebaut.

{% callout title="Tipp" %}
Über den Button `Zum Online-Shop` gelangen Sie zu Ihrem Online-Shop.
{% /callout %}

Die Übersicht ist als Tabelle aufgebaut. Links sehen Sie, ob die Seite aktiv ist und dementsprechend in Ihrem Online-Shop sichtbar ist.
Daneben steht der Name der Seite. Sie können den Titel, bzw. Namen der einzelnen Seiten ändern.
Rechts ist das kleine Icon `Kompass`, klicken Sie darauf, gelangen Sie zu der jeweiligen Seite in Ihrem Online-Shop.
Daneben ist noch das kleine Icon `Schraubenschlüssel`, klicken Sie darauf, wenn Sie die Seite bearbeiten wollen.

## Seite Bearbeiten

Wenn Sie die Seite bearbeiten möchten, klicken Sie rechts auf den kleinen `Schraubenschlüssel`.

#### Schritte

1. Geben Sie der Seite einen Titel, z.B. Allgemeine Geschäftsbedingungen.
1. Entscheiden Sie, ob die Seite im Shop angezeigt werden soll mit einem Klick in das kleine Kästchen bei `Sichtbar`.
1. Tragen Sie mit Hilfe des Text Editors Ihren Text ein.
1. Geben Sie der Seite Keywords (kurze Schlagwörter). Diese werden in der internen Suche berücksichtigt und kann Ihr Suchmaschinen Ranking verbessern.
1. Fügen Sie noch eine kurze Beschreibung (max. 155 Zeichen) ein. Diese Beschreibung erscheint bei einer Suchanfrage als Kurzbeschreibung.
1. Klicken Sie auf `Übernehmen` um die Änderung erstmal zu speichern und dann an dieser Seite weiter zu arbeiten.
1. Klicken Sie auf `Speichern & Schließen` um die Änderung zu speichern und zur Überischt zurückzukehren.

[Hier bekommen Sie unsere ausführliche Anleitung zu dem Texteditor](/de/assets/wysiwyg.html).

{% callout type="warning" title="Hinweis" %}
Dieser Bereich ist ausschließlich für rechtliche Hinweise gedacht. Sollten die vorgegebenen Seiten nicht ausreichen, kontaktieren Sie bitte das Online-Support Team.
{% /callout %}

## Online-Support

Telefon: +49(0)221.2228950  
Email: online-support@stilpunkte.de

## Bedingungen

- Alle Pflichtfelder sind mit einem `*` versehen.
- Geben Sie der Seite eine aussagekräftige META Beschreibung und Keywords und versuchen Sie, sich an die vorgegeben Länge der Zeichen zu halten.
