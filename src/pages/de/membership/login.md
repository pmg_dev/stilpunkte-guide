---
title: Kundenlogin
---

Die Verwaltung ist Ihr persönlicher, geschützter Bereich, in dem Sie Änderungen an Ihrem STILPUNKTE® Eintrag vornehmen und Ihren STILPUNKTE® Online-Shop verwalten können. Hier finden Sie auch unser zusätzliches Angebot rund um Ihren STILPUNKTE® Eintrag. {% .lead %}

Um zu Ihrem Kundenbereich zu gelangen, besuchen Sie diesen Link: [stilpunkte.de/verwaltung/login](https://www.stilpunkte.de/verwaltung/login/) und melden Sie sich mit Ihren Zugangsdaten an. Ihre Zugangsdaten, bestehend aus Benutzer und Passwort, bekommen Sie vom STILPUNKTE® Online-Support per Email zugestellt.

Den Link zum Kundenbereich finden Sie auch im Footer auf der STILPUNKTE® Seite: `Kundenlogin`.

## Eintrag

Sie haben die Möglichkeiten den Eintrag Ihres Unternehmens auf STILPUNKTE® selbst zu gestalten.

Folgende Einstellungen können Sie selbst vornehmen:
- Fotos hochladen
- Reihenfolge der Fotos ändern
- eine Unternehmensbeschreibung erstellen
- Angebote/Events erstellen

Um Änderungen an Ihrem Eintrag vorzunehmen, klicken Sie im Menü auf `Home → Eintrag`.

### Eintrag bearbeiten

Dieser Bereich gibt Ihnen die Möglichkeit Ihren Eintrag im STILPUNKTE® Online-Portal zu bearbeiten.

Der Eintrag soll dem Kunden ein Gesamtbild Ihres Unternehmens vermitteln. Sie können Bilder hochladen, eine Unternehmensbeschreibung und Angebote/Events einstellen. Das STILPUNKTE® Team ergänzt Ihren Eintrag mit Kontaktdaten, Öffnungszeiten, Ihrem Logo und führt ihre Marken und Kategorien auf. So entsteht ein stimmiger erster Gesamteindruck. Auf Wunsch können wir Ihren Eintrag auch mit ihren Social Media Kanälen Facebook, Instagram, Pinterest, Twitter, Youtube und Xing verbinden.

#### Schritte:

1. Klicken Sie auf `Bearbeiten`.
1. Fügen Sie durch klicken auf `Headline` eine Überschrift Ihrer Wahl ein und drücken Sie 'Enter' auf Ihrer Tastatur.
1. Ein neues Textfeld öffnet sich.
1. Tragen Sie Ihren Wunschtext ein.
1. Klicken Sie auf `Speichern` wenn Sie fertig sind. Bei Klicken auf `Abbrechen` werden alle Änderungen verworfen.

Die Anzahl an Textfeldern und Headlines können Sie beliebig wählen.
[Hier bekommen Sie unsere ausführliche Anleitung zu dem Texteditor](/de/assets/wysiwyg).

### Fotos hochladen und Reihenfolge ändern

Sie haben die Möglichkeit Ihren Eintrag mit Fotos zu ergänzen und persönlicher zu gestalten, Ihr Unternehmen, Sie selbst und Ihr Team vorzustellen.

#### Schritte

1. Klicken Sie auf `Bearbeiten`.
1. Ziehen Sie entweder per Drag & Drop ein Bild von Ihrem Desktop in das Fenster `neues Bild hochladen` oder klicken Sie in das Fenster und laden ein Bild von Ihrem Computer hoch.
1. Verschieben Sie Fotos indem Sie das Foto mit der linken Maustaste auswählen, Taste gedrückt halten und das Foto so an die richtige Stelle ziehen.
1. Wollen Sie ein Foto löschen, ziehen Sie es mit gedrückter linker Maustaste in den Kasten `zum Löschen hier ablegen`.
1. Änderungen werden automatisch gespeichert, außer Sie gehen auf den Button `Zurück`.  

{% callout type="warning" title="Hinweis" %}
Bitte beachten Sie, dass das erste Bild vom STILPUNKTE® Team eingestellt wird und von Ihnen nicht verschoben oder gelöscht werden kann. Möchten Sie Änderungen beim Startbild vornehmen, wenden Sie sich bitte an das STILPUNKTE® Online-Support Team.
{% /callout %}

### Angebote/Events bearbeiten

Sie haben die Möglichkeit Angebote/Events oder Sonderaktionen auf Ihrer Eintragsseite anzuzeigen.

{% callout type="warning" title="Hinweis" %}
Bitte beachten Sie, dass diese Einstellung nur Premium Partnern vorbehalten ist.
{% /callout %}

#### Schritte

1. Klicken Sie auf den Button `bearbeiten`. Jetzt bietet Ihnen das Fenster mehrere Ausfüllmöglichkeiten.
1. Wählen Sie im oberen Feld eine passende Überschrift, die Ihre Sonderaktion passend beschreibt.
1. Ziehen Sie entweder per Drag & Drop ein Foto in den Bereich oder klicken Sie auf das kleine Icon rechts oben und fügen ein Bild über Ihren Computer hinzu. Die optimale Bildgröße beträgt `520 x 164 px` .
1. Tragen Sie eine Kurzbeschreibung (max. 315 Wörter) ein.
1. Sie können dem Angebot eine begrenzte Laufzeit geben oder direkt einen Zeitraum eintragen, in dem das Angebot gültig ist.
1. Geben Sie die genaue Beschreibung des Angebotes an. Sie haben die Wahl zwischen Angebot, Gutschein, Event, Veranstaltung oder Rabatt.
1. Lassen Sie das Angebot über die Buttons `Eintrag aktiv` und `Eintrag inaktiv` in Ihrem Eintrag sichtbar werden oder verschwinden.

## Online-Shop

Im Kundenlogin können Sie Ihren STILPUNKTE® Online-Shop einstellen und verwalten. [Hier bekommen Sie unsere ausführliche Anleitung zu Ihrem STILPUNKTE® Online-Shop](/de/shops/).

Sollten Sie noch keinen Online-Shop von STILPUNKTE® haben, bewerben Sie sich [hier.](https://www.stilpunkte.de/mein-eigener-online-shop/)

## Dokumentation

Ihnen steht eine ausführliche Dokumentation rund um unser gesamtes STILPUNKTE® -Portal zur Verfügung. 

Die Dokumentation hilft Ihnen alles rund um unser STILPUNKTE® -Portal besser zu verstehen und soll Sie als `Schritt für Schritt`-Anleitung bei der Bearbeitung Ihres Eintrages oder der Erstellung Ihres eigenen STILPUNKTE® -Online-Shops unterstützen und begleiten.

Sie gelangen einfach mit einem Klick auf den Link `Zur Dokumentation` zur Startseite der Dokumentation. Hier stehen Ihnen schon die Links zu den wichtigsten Bereichen zur Verfügung.
In der linken Spalte finden Sie das Inhaltsverzeichnis. Per Klick auf ein einzelnes Thema gelangen Sie direkt dorthin. Oben rechts können Sie aber auch nach einem bestimmten Thema oder Wort suchen. Von hier aus führt auch ein Link wieder zum STILPUNKTE® -Portal.

## Auszeichnung

Banner sind eine schöne Möglichkeit Kunden auf etwas gezielt aufmerksam zu machen. Präsentieren Sie Ihren Kunden, dass Sie als STILPUNKT® ausgezeichnet wurden und heben Sie sich von der Masse ab. Unter dem Link `Ansehen` finden Sie den HTML-Code mit dem Sie die STILPUNKTE® -Auszeichnung auf Ihrer Webseite einbinden können.

### Schritte

1. Kopieren Sie den Code in Ihre Zwischenablage.
2. Binden Sie den HTML-Code auf Ihrer Webseite ein.

## Werbeartikel

STILPUNKTE® bietet Ihnen auch ein breites Portfolio an Werbeartikeln. Mit einem Klick auf `Zur Bestellung` gelangen Sie zu einer Übersicht und unserem Bestellformular.
Wir bieten Ihnen:
- Urkunde A4 (bestellbar für alle STILPUNKTE® -Partner)
- Urkunde A3 (bestellbar für alle STILPUNKTE® -Partner einer Premium- und Premium Plus-Mitgliedschaft)
- Flyer mit und ohne Co-Branding
- Aufsteller für Flyer und Bewertungskarten
- Bewertungskarten mit Co-Branding (Ihr Logo sowie ein QR-Code, der direkt zu Ihrem Eintrag führt)
- Bewertungskarten ohne Co-Branding (mit QR-Code zur STILPUNKTE®-Startseite)
- Lesezeichen mit Ihrem Logo und Ihren Fotos
- Displayständer mit und ohne Co-Branding
- Aufkleber in verschiedenen Größen für Innen und Außen

Wählen Sie einfach die gewünschten Artikel aus und klicken auf `Jetzt bestellen`. Eine gesamte Bestellübersicht wird Ihnen per E-Mail zugeschickt. Rechnungs- und Lieferadresse werden aus Ihrem STILPUNKTE®-Eintrag übernommen.

## Kundenbewertungen

Sie sind besonders stolz auf Ihre Kundenbewertungen und möchten Sie mit der ganzen Welt teilen? Hier finden Sie den HTML-Code, den Sie auf Ihrer Webseite einbinden müssen, damit Sie Ihre Kundenbewertungen auf Ihrer eigenen Webseite präsentieren können und die Bewertungssterne auch bei den Google-Suchanzeigen an den Unterseiten Ihrer eigenen Webseite angezeigt werden.

### Schritte

1. Kopieren Sie den Code in Ihre Zwischenablage.
2. Binden Sie den HTML-Code auf Ihrer Webseite ein.

Eine ausführliche Beschreibung der STILPUNKTE®-Kundenbewertungen finden Sie [hier.](https://docs.stilpunkte.de/de/overview/customer-review.html)

---

## Support

Sollten Sie noch weitere Fragen haben, zögern Sie nicht und rufen uns gerne an oder senden Sie Ihre Anfrage per Mail.

Telefon: +49(0)221.2228950  
E-Mail: [online-support@stilpunkte.de](mailto:online-support@stilpunkte.de)
