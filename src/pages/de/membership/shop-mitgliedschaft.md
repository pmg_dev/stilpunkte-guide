---
title: Shop-Mitgliedschaft
---

Die STILPUNKTE® Shop-Mitgliedschaft ist von uns für Sie entwickelt worden, um Ihnen einen einfachen Online-Shop anbieten zu können. Des Weiteren erhalten Sie zu folgenden Leistungen vom STILPUNKTE® Online-Portal Zugang:

- 1 Bild (Querformat)
- Platzierung Ihres Firmenlogos
- Anschrift / Telefon
- E-Mail Button
- Webseitenverlinkung
- Shop-Verlinkung
- Kunden-Login zur Bearbeitung Ihres Eintrags sowie Online-Shops
- Social Media-Verlinkung
  - Facebook
  - Instagram
  - Xing
  - Pinterest
  - Youtube
  - Twitter
  - LinkedIn
  - TikTok
- 4 Kategorien / 4 Marken
- Verlinkung auf STILPUNKTE® App
- Download des STILPUNKTE® Points

---

## Nähere Informationen

Für nähere Informationen steht Ihnen unser Support Team gerne zur Verfügung:

Telefon: +49(0)221.2228950  
E-Mail: [online-support@stilpunkte.de](mailto:online-support@stilpunkte.de)
