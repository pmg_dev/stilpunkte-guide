---
title: Standard Online-Mitgliedschaft
---

- 1 Bild (Querformat)
- Anschrift & Telefon
- 4 Kategorie
- 4 Marken
- STILPUNKTE® Point und Auszeichnung für die Geschäftsräume  
  (ohne weitere werbliche Nutzung der Marke STILPUNKTE®)

Ein Beispiel finden Sie [hier](https://www.stilpunkte.de/koeln/eintraege/stilpunkte-lifestyle-guide/).

---

## Nähere Informationen

Für nähere Informationen steht Ihnen unser Support Team gerne zur Verfügung:

Telefon: +49(0)221.2228950  
E-Mail: [online-support@stilpunkte.de](mailto:online-support@stilpunkte.de)
