---
title: Professional Online-Mitgliedschaft
---

Alle Leistungen der [Shop-Mitgliedschaft](./shop-mitgliedschaft), **plus**:

- 5 weitere Bilder in der Slideshow (Querformat, insgesamt 6)
- Ersttexteinpflege durch das STILPUNKTE® Team, Text wird vom Kunden erstellt
- Webseitenverlinkung
- bis zu 20 weitere Kategorien (insgesamt 24)
- bis zu 28 weitere Marken (insgesamt 32)
- Google Maps-Anbindung
- Öffnungszeiten
- Kunden-Login zur Bearbeitung Ihres Eintrags, Ihrer Texte und Bilder
- Social Media Verlinkung
  - Facebook
  - Instagram
  - Xing
  - Pinterest
  - Youtube
  - Twitter
  - LinkedIn
  - TikTok
- Download des STILPUNKTE® Point
- Werbliche Nutzung der Auszeichnung (Urkunde und STILPUNKTE® Point) auf Ihrer Webseite und Ihren Social Media Seiten
- STILPUNKTE® Online Shop kostenfrei und in Ihrem Unternehmens CI
- 1.000 Co-Gebrandete STILPUNKTE®-Bewertungskarten

Ein Beispiel finden Sie [hier.](https://www.stilpunkte.de/koeln/eintraege/stilpunkte-lifestyle-guide-1/)

---

## Nähere Informationen

Für nähere Informationen steht Ihnen unser Support Team gerne zur Verfügung:

Telefon: +49(0)221.2228950  
E-Mail: [online-support@stilpunkte.de](mailto:online-support@stilpunkte.de)
