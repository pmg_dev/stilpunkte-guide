---
title: Standard Pro Mitgliedschaft für Einzelhändler
---

- 3 Bilder (Querformat)
- Anschrift / Telefon
- E-Mail Verlinkung
- Webseitenverlinkung
- Online Shop-Verlinkung
- Social Media-Verlinkung
  - Facebook
  - Instagram
  - Xing
  - Pinterest
  - Youtube
  - Twitter
  - LinkedIn
  - TikTok
- 4 Kategorien / 12 Marken
- Platzierung Ihres Firmenlogos
- Verlinkung auf die STILPUNKTE® App
- 3 Produkte können jederzeit über den Kundenlogin gewechselt werden
- Download des STILPUNKTE® Point
- Werbliche Nutzung der Auszeichnung (Urkunde und STILPUNKTE® Point) auf Ihrer Webseite und Ihren Social Media Seiten

---

## Nähere Informationen

Für nähere Informationen steht Ihnen unser Support Team gerne zur Verfügung:

Telefon: +49(0)221.2228950  
E-Mail: [online-support@stilpunkte.de](mailto:online-support@stilpunkte.de)
