---
title: Kundenbewertungen
meta:
  - name: og:title
    content: Kundenbewertungen
---

Das STILPUNKTE® Online-Portal bietet Konsumenten die Möglichkeit, STILPUNKTE® -Partner zu bewerten. 

## Allgemein

Die Kundenbewertungen erscheinen auf Ihrer Eintragsseite, unterhalb Ihrer Adressdaten. 

![](/images/overview/customer_review_01.jpg)

Hier werden die vergebenen Sterne und Anzahl der Bewertungen angezeigt. Darunter erscheint der Button, der den Kunden zur Bewertungsmaske führt.
Unterhalb Ihres Google Maps Eintrages wird ein weiterer Button angezeigt, der den Kunden zur Bewertungsmaske führt. Hier werden ebenfalls die vollständigen Bewertungen dargestellt.

![](/images/overview/customer_review_02.jpg)

## Ablauf

Möchte ein Kunde Ihr Unternehmen bewerten, muss dieser mehrere Schritte durchlaufen.

![](/images/overview/customer_review_03.jpg)

Zuerst muss der Konsument die Anzahl der Sterne (1-5) auswählen, die er vergeben möchte. Zusätzlich muss eine Bewertung geschrieben werden. Dies ist **nicht** optional.

![](/images/overview/customer_review_04.jpg)

Der Konsument muss nun seinen Namen eingeben, der mit der Bewertung veröffentlicht wird.

![](/images/overview/customer_review_05.jpg)

Danach ist der Konsument dazu verpflichtet eine E-Mail Adresse einzugeben. Optional kann er sich hier auch zum Newsletter anmelden.

![](/images/overview/customer_review_06.jpg)

![](/images/overview/customer_review_07.jpg)

Die E-Mail Adresse wird nicht veröffentlicht, aber die Bewertung muss per E-Mail verifiziert werden. Dies dient zur Sicherheit. Es ist außerdem nicht möglich mit ein und derselben E-Mail Adresse mehrere Bewertungen für das selbe Unternehmen abzugeben.

![](/images/overview/customer_review_08.jpg)

Nach Bestätigung der Bewertung durch den Konsumenten wird diese veröffentlicht. Gleichzeitig bekommen Sie eine E-Mail, die Sie über die Bewertung informiert und Ihnen auch die Möglichkeit gibt, auf die Bewertung zu antworten und mit dem Kunden in Kontakt zu treten.

## Einstellungen

Ihre Benachrichtungsmail geht an die E-Mail Adresse, die für Ihren Eintrag hinterlegt ist. Sollten Sie dafür eine andere E-Mail Adresse favorisieren, wenden Sie sich bitte an den STILPUNKTE® Online-Support.

Sie haben auch die Möglichkeit, die Bewertungssterne auf Ihrer eigenen Webseite und den Google-Suchergebnissen einzupflegen. Den dafür benötigten HTML Code finden Sie [hier.](https://www.stilpunkte.de/verwaltung/start/kundenbewertungen/) 

{% callout type="warning" title="Hinweis" %}
  Falls Sie keine Kundenbewertungen in Ihrem Eintrag wünschen, wenden Sie sich bitte an den STILPUNKTE® Online-Support.

  Telefon: +49(0)221.2228950  
  E-Mail: [online-support@stilpunkte.de](mailto:online-support@stilpunkte.de)
{% /callout %}
