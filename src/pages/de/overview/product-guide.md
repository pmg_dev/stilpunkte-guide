---
title: Product Guide
---

## Situation

Das Kaufverhalten unterliegt einem ständigen Wandel. Einerseits wird der persönliche Kontakt zu qualifizierten Verkäufern und die damit verbundene Beratung geschätzt, andererseits verbringen immer mehr Menschen ihre Zeit im Internet. Gerade durch die mobile Kommunikation ist Internet heute immer mit dabei und auf Tastendruck verfügbar. Plattformen wie Amazon, Fashion ID u.v.m. freuen sich über große Besucherzahlen, denn gerade bei Markenprodukten handelt es sich immer um dieselbe Ware – ob online oder offline gekauft.

Seit Jahren ächzt der traditionelle stationäre Einzelhandel unter der zunehmenden Konkurrenz durch Online-Shops. Amazon, Zalando und Co. haben dafür gesorgt, dass immer weniger Verbraucher zum Einkaufen ihre eigenen vier Wände verlassen. So gut wie alles finden wir mittlerweile online
in nahezu unbegrenzter Vielfalt; und dank der Einsparungen, die der Verzicht auf lokale Repräsentanzen dem Online-Handel ermöglicht, kann dieser die Ladenpreise des Einzelhandels in der Regel deutlich unterbieten.

> [...] die Online-Händler werden dem klassischen Einzelhandel in den nächsten Jahren immer mehr und immer schneller Umsätze wegnehmen.

### Daten, Zahlen, Fakten

"Die Schleuse ist offen. Die Online-Händler werden dem klassischen Einzelhandel in den nächsten Jahren immer mehr und immer schneller Umsätze wegnehmen“, prognostiziert Gerrit Heinemann, Handelsexperte und Leiter des eWeb-Research-Center an der Hochschule Niederrhein. „Viele Händler werden wohl ihre Läden schließen müssen." Nach einer aktuellen Umfrage des Kölner Instituts für Handelsforschung (IFH) hat bereits jeder dritte Verbraucher die Anzahl der Fahrten ins Stadtzentrum verringert und kauft stattdessen öfter im Internet ein. Über 60 Prozent der "normalen" Einzelhändler klagen nach Angaben des Einzelhandelsverbandes Deutschland (HDE) über sinkende Besucherzahlen in ihren Geschäften. Doch das ist wohl erst der Anfang. Was da auf viele klassische Einzelhändler zukommt, lässt der Verlauf des vergangenen Weihnachtsgeschäfts erahnen. Denn während die Einzelhandelsumsätze in den wichtigsten Verkaufsmonaten des Jahres nach Angaben des HDE bei 80 Milliarden Euro stagnierten, explodierten die Umsätze der Online-Händler. Sie verzeichneten nach Angaben des Bundesverbandes des Deutschen Versandhandels (BVH) ein Wachstum in Höhe von 54,5 % auf 8,5 Milliarden Euro.

> [...] jeder dritte Verbraucher [...] hat [...] die Anzahl der Fahrten ins Stadt-Zentrum verringert und kauft stattdessen öfter im Internet ein.

### Akuter Handlungsbedarf für den Einzelhandel - offline vs. online

#### Trends

Beim Handel mit Büchern, Elektronik und Kleidung hat sich das Online-Angebot bereits durchgesetzt. Aber auch bei Lebensmitteln sind schon die ersten Plattformen online. Auch im kommenden Jahr wird der Online-Handel nach Einschätzungen des HDE mehr als zehnmal so stark wachsen wie der Einzelhandel insgesamt. In weniger als zehn Jahren werde ein Viertel aller Einkäufe im Internet erledigt, erwartet Handelsexperte Gerrit Heinemann.

> [...] im Internet [...] werden derzeit hauptsächlich große Unternehmen gefunden, weil der Einzelhändler nur sich selbst, nicht aber seine Produkte online präsentiert.

## Problem

Das Hauptproblem ist, dass die meisten Einzelhandelsunternehmen mit Ihren Produkten im Internet nicht sichtbar sind. Sie können ihre Qualitäten und Vorteile nicht ausspielen, denn die Kaufentscheidung von Internetnutzern wird überwiegend zuhause getroffen, und zwar häufig nach Ladenschluss, am Feierabend oder während des Wochenendes. Gerade in der heutigen Zeit ist es enorm wichtig, auch online Präsenz zu zeigen und den Konsumenten die Vorteile aufzuzeigen, die ein Kauf im Geschäft vor Ort bringt. Nur wenn ein Ladengeschäft über Suchmaschinen gefunden wird und online verfügbar und erlebbar ist, kann es auf Dauer neben der Online-Konkurrenz bestehen.

Die Folge der übermächtigen Konkurrenz durch eCommerce-Unternehmen sind massive Umsatzeinbußen im Einzelhandel und vielfach verwaiste Einkaufsstraßen in den Städten. Abgesehen von den großen multinationalen Ketten, die meist auch über eigene Online-Shops verfügen, kämpft der Einzelhandel vielerorts ums Überleben.

Wenn es um das Online-Geschäft ging, beschränkte sich der Einzelhandel lange nur auf eigene Webseiten. Doch die Grenzen zwischen Internet und Fußgängerzone verschwimmen immer mehr – nicht nur für die stationären Händler. Für inhabergeführte Ladengeschäfte ist die zusätzliche Führung und Pflege von Online-Shops oft zu kosten- und zeitintensiv.

Das Konsumverhalten des anspruchsvollen Konsumenten ist jedoch vor allem dadurch gekennzeichnet, dass Produkte im Internet gesucht werden, um Zeit zu sparen. Hierbei werden derzeit hauptsächlich große Unternehmen gefunden, weil der Einzelhändler allenfalls sich selbst, nicht aber seine Produkte online präsentiert. Der anspruchsvolle Konsument würde wohl in vielen Fällen der individuellen Beratung durch den Einzelhändler den Vorzug geben, das Produkt vor Ort begutachten und gegebenenfalls sofort mitnehmen. Da der Einzelhändler jedoch nicht über die Produkt-Suche gefunden wird, kauft der Konsument dann häufig online bei einem der bekannten Anbieter im Internet.

> Die Folge der übermächtigen Konkurrenz durch eCommerce-Unternehmen sind massive Umsatzeinbußen im Einzelhandel und vielfach verwaiste Einkaufsstraßen in den Städten.

## Lösung

**Unsere Lösung für den Einzelhandel: Der STILPUNKTE® Product Guide**

Online finden – online oder telefonisch reservieren – oder gleich abholen. Wir stellen uns gegen die Marktmacht des Online-Handels und eröffnen mit unserem STILPUNKTE® Product Guide eine neue Generation des Shoppens: Online = Offline. Wer künftig sein Wunschprodukt im Product Guide findet, muss nicht mehr zu Hause auf den Postboten warten und Umtauschrisiken in Kauf nehmen, sondern kann sein Wunschobjekt direkt im Ladengeschäft reservieren, vor Ort begutachten und selbst abholen. Die Idee ist, die Grenzen zwischen Online-Handel und stationärem Einzelhandel verschwimmen zu lassen.

### Ihre Vorteile

Bessere Auffindbarkeit Ihres Ladengeschäftes bei Google & Co. durch die Platzierung von Produkten, nach denen gesucht wird. Wir unterstützen dies durch unsere SEO-Maßnahmen (Suchmaschinenoptimierung) und eine eigene Landingpage für jedes einzelne Produkt.

> Der Product Guide ist einfach zu handhaben und für jeden erschwinglich.

> Der STILPUNKTE® Product Guide ist Ihr erweitertes Schaufenster im Internet

> Online den Kunden ins stationäre Ladengeschäft locken [...]

> Sie können Ihre Produkte direkt auf Ihren Online-Shop verlinken

> Sie werden durch den STILPUNKTE® Product Guide gefunden und gewinnen neue Kunden

> Der Kontakt-Button bringt den Kunden direkt mit Ihnen in Kontakt

> War der Kunde erst einmal in Ihrem Geschäft, wird er zumeist wiederkommen.

### Die Product Guide Themenübersicht

Das Anklicken des Product Guide unter [www.stilpunkte.de](https://www.stilpunkte.de/deutschland/produkte/1/1/) öffnet zunächst die Themenübersicht. Ein Klick führt in den ausgewählten Themenbereich hinein, in dem die einzelnen Produktvorschläge nach dem Zufallsprinzip rotierend angeordnet sind. Zuletzt eingestellte Produkte werden dabei jeweils oben angezeigt.

Die zur Auswahl stehenden Unterkategorien ermöglichen eine praxisgerechte Filterung der angezeigten Produkteinträge.

### Die Product Guide Produktseite

Durch die Auswahl eines Produktes, öffnet sich die Produktseite mit bis zu 5 Bildern, Beschreibungstext und Bezugsquelle, d.h. den Kontaktdaten des Ladengeschäftes.

### Die STILPUNKTE® Händlerseite

Darunter befinden sich Produktvorschläge aus den Bereichen „Ähnliche Produkte“ und „Weitere Produkte des gleichen Händlers“. Ein Klick auf das Informationsfeld unter den Kontaktdaten führt zur Händlerseite mit der Google Maps-Karte, weiteren angebotenen Produkten und Marken des Händlers und, sofern hinterlegt, einem Link zu dessen Online-Shop. Jede Seite bietet ein Eingabefeld zur Produkt- oder Markensuche. Die Suchfunktion ist interaktiv, d.h. vorschlagunterstützt während des Eintippens. Über die Produkt- oder Markensuche wird der Händler gefunden.

### Ihr persönlicher Bereich im Product Guide

Für die Eintragung und Verwaltung Ihrer Produkte steht Ihnen ein passwortgeschützter Bereich des Product Guide zur Verfügung, der nur Ihnen und unserem Support-Team zugänglich ist. Von der Startseite aus erhalten Sie über die Navigationspunkte „Eintrag“ und „Produkte“ Zugriff auf Bilder, Angebote, Texte und Produkteinträge Ihrer persönlichen STILPUNKTE®-Präsenz.

#### Produktübersicht

Auf dieser Seite können Sie Ihre Produkteinträge komfortabel verwalten, neue Einträge anlegen und alte entfernen. Über einen Button gelangen Sie in das Bearbeitungsmenü zur Neuanlage oder zur Bearbeitung eines bestehenden Produktes.

#### Bearbeitungsmenü

Im Bearbeitungsmenü steuern und verwalten Sie zentral alle Titel, Texte, Bilder, Suchbegriffe, Maße, Kategorien und die gewünschte Laufzeit Ihrer Produktpräsentation.
