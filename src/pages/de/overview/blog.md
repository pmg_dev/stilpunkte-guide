---
title: Redaktionelle Berichte im STILPUNKTE® Blog
---

Der Blog-Bericht wird von der STILPUNKTE® Redaktion verfasst und inhaltlich mit Ihnen abgestimmt. Das Bildmaterial wird von Ihnen zur Verfügung gestellt. Blog-Berichte beinhalten keine plakativen Werbe-Slogans, sondern erhalten ihre Glaubwürdigkeit bei den Lesern durch einen berichtenden Schreibstil in der dritten Person, ganz im Sinne einer redaktionellen Empfehlung. Der Bericht im STILPUNKTE® Blog ist damit eine perfekte Ergänzung zu Ihren Präsenz-Seiten im STILPUNKTE® Portal.

## Der Bericht setzt sich zusammen aus

- Headline, Subline, 1 Kategoriezuordnung, 1 Titelbild und bis zu 7 weitere Bilder
- Textzeichen inkl. Leerzeichen mindestens 2.500
- Verlinkung mit Ihrer Präsenzseite im STILPUNKTE®-Portal
- Verlinkung mit Ihrer Webseite
- Social Media Verlinkung: Facebook, Instagram, Pinterest, Twitter, Xing 
  (Blog-Berichte können auf Social Media Seiten geteilt werden)
- Social Media Bewerbung: Facebook | Instagram | Pinterest | Twitter
- Text, Bilder und verdeckte Felder werden für Suchmaschinen optimiert (SEO)
- Blog-Berichte sind chronologisch sortiert und im Portal konstant sichtbar
- Filterung nach Kategorien
