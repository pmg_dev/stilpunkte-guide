import Head from "next/head";
import Link from "next/link";

export default function NotFound() {
  return (
    <>
      <Head>
        <title>Page not found - STILPUNKTE Docs</title>
      </Head>
      <div className="flex h-full flex-col items-center justify-center text-center">
        <p className="font-medium font-display text-sm text-gray-900 dark:text-white">
          404
        </p>
        <h1 className="mt-3 font-display text-3xl tracking-tight text-gray-900 dark:text-white">
          Seite nicht gefunden
        </h1>
        <p className="mt-2 text-sm text-gray-500 dark:text-gray-400">
          Leider konnten wir die von Ihnen gesuchte Seite nicht finden.
        </p>
        <Link
          href="/"
          className="font-medium mt-8 text-sm text-gray-900 dark:text-white"
        >
          Zurück zur Startseite
        </Link>
      </div>
    </>
  );
}
