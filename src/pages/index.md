---
title: Einführung
pageTitle: STILPUNKTE® Dokumentation
description: 
---

{% quick-links %}
  {% quick-link 
    title="Mitgliedschaften" 
    icon="installation" 
    href="/de/membership" 
    description="Unsere Online-Mitgliedschaften im kurzen Überblick." 
  /%}
  {% quick-link 
    title="Online-Shops" 
    icon="presets" 
    href="/de/shops" 
    description="Ihr eigener Online-Shop ganz einfach erstellt." 
  /%}
{% /quick-links %}

---

## Kurze Vorstellung

STILPUNKTE® ist ein crossmedialer Lifestyle-Guide für Konsumenten mit exklusivem Lebensstil. Nur die besten Adressen, ausgewählte redaktionelle Beiträge sowie ausschließlich erstklassige Produkte und Dienstleistungen finden Sie auf www.stilpunkte.de und in den dazugehörigen Print-Magazinen.
STILPUNKTE® präsentiert Ihnen als einer der größten Marken-Finder Deutschlands mit stetigem Wachstum ausgesuchte Adressen, besondere Markenprodukte sowie Dienstleistungen und dazu die STILPUNKTE® Online Shops für Mitglieder.
Sie möchten heute direkt vor Ort shoppen und morgen doch lieber bequem von zuhause – alles ist möglich – nutzen Sie einfach unseren Shop- und Marken-Finder um Ihre persönlichen STILPUNKTE® in Ihrer Nähe zu suchen oder unsere STILPUNKTE® Online Shopping-Mall für den spontanen Online-Einkauf.

Neben besten Adressen finden Sie auf unserem STILPUNKTE® Portal ebenso exklusive Angebote und Events, exzellente Produktempfehlungen in unserem PRODUCT GUIDE, Aktuelles im STILPUNKTE® Blog, dazu feine kulinarische Rezepte, inspirierende Reiseberichte, stilsichere Hotelempfehlungen und ausgewählte redaktionelle Beiträge.

Das Ziel von STILPUNKTE® ist es, im deutschsprachigen Europa größter Online Lifestyle Guide für Konsumenten mit anspruchsvollem Konsumverhalten und hohem Haushaltseinkommen zu werden.

STILPUNKTE® Lifestyle-Guide und die STILPUNKTE® Online Shopping-Mall bilden gemeinsam ein zukunftsgerichtetes Konzept für exklusive und besondere Markenprodukte.

Für Sie sind wir stets auf der Suche nach dem Besonderen in Ihrer Stadt und informieren Sie ganz aktuell per Newsletter oder jetzt auch mit der [STILPUNKTE® App](/de/overview/app/).

## STILPUNKTE® Magazin

In diesen Regionen erscheint der STILPUNKTE® Lifestyle Guide:

- Bergisches Land
- Berlin / Brandenburg
- Düsseldorf / Niederrhein / Aachen
- Hamburg / Sylt
- Köln / Bonn / Rhein-Sieg
- Ruhrgebiet

Das Lifestyle-Magazin können Sie sich bequem nach Hause liefern lassen, oder [online durchstöbern](https://www.stilpunkte.de/emagazine/).

## STILPUNKTE® Online-Portal

In STILPUNKTE® Online-Portal sind folgende Regionen vertreten:

- [Aachen](https://www.stilpunkte.de/aachen/portal/)
- [Augsburg](https://www.stilpunkte.de/augsburg/portal/)
- [Bergisches Land](https://www.stilpunkte.de/bergisches-land/portal/)
- [Berlin / Brandenburg](https://www.stilpunkte.de/berlin/portal/)
- [Bochum](https://www.stilpunkte.de/bochum/portal/)
- [Bonn Rhein-Sieg](https://www.stilpunkte.de/bonn/portal/)
- [Bremen / Oldenburg](https://www.stilpunkte.de/bremen/portal/)
- [Düsseldorf](https://www.stilpunkte.de/duesseldorf/portal/)
- [Dortmund](https://www.stilpunkte.de/dortmund/portal/)
- [Essen](https://www.stilpunkte.de/essen/portal/)
- [Frankfurt / Rhein-Main](https://www.stilpunkte.de/frankfurt/portal/)
- [Hamburg](https://www.stilpunkte.de/hamburg/portal/)
- [Hannover / Braunschweig](https://www.stilpunkte.de/hannover/portal/)
- [Köln](https://www.stilpunkte.de/koeln/portal/)
- [Kiel / Flensburg](https://www.stilpunkte.de/kiel/portal/)
- [Koblenz](https://www.stilpunkte.de/koblenz/portal/)
- [Lübecker Bucht](https://www.stilpunkte.de/luebecker-bucht/portal/)
- [Lüneburger Heide](https://www.stilpunkte.de/lueneburger-heide/portal/)
- [Leipzig](https://www.stilpunkte.de/leipzig/portal/)
- [Mönchengladbach](https://www.stilpunkte.de/moenchengladbach/portal/)
- [München](https://www.stilpunkte.de/muenchen/portal/)
- [Münster / Münsterland](https://www.stilpunkte.de/muenster/portal/)
- [Niederrhein](https://www.stilpunkte.de/niederrhein/portal/)
- [Osnabrück / Ems / Vechte](https://www.stilpunkte.de/osnabrueck/portal/)
- [Ostwestfalen-Lippe](https://www.stilpunkte.de/ostwestfalen-lippe/portal/)
- [Ruhrgebiet-Ost](https://www.stilpunkte.de/ruhrgebiet-ost/portal/)
- [Ruhrgebiet-West](https://www.stilpunkte.de/ruhrgebiet-west/portal/)
- [Südwestfahlen](https://www.stilpunkte.de/suedwestfalen/portal/)
- [Siegerland](https://www.stilpunkte.de/siegerland/portal/)
- [Stuttgart](https://www.stilpunkte.de/stuttgart/portal/)
- [Sylt](https://www.stilpunkte.de/sylt/portal/)
- [Thüringen](https://www.stilpunkte.de/erfurt/portal/)
- [Wuppertal](https://www.stilpunkte.de/wuppertal/portal/)
