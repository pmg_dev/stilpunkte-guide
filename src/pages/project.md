---
meta:
  - name: robots
    content: noindex, nofollow
---
# Projekt

## Projektbeschreibung

Aufgebaut wird ein Online-Marktplatz Service, auf dem Händler kostenlos einen autarken Online-Shops erstellen und verwalten können. Jeder Händler bekommt eine passwortgeschützte Web-Oberfläche, in dem er seine Produkte und Bestellungen einfach und ohne HTML-Kenntnisse verwalten kann - ohne selbst eine Infrastruktur aufbauen und warten zu müssen.

Der Online-Marktplatz, sowie die autarken Online-Shops werden auf dem Desktop-PC, sowie auf mobilen Endgeräten gleichermaßen gut erreichbar und nutzbar sein. Hierfür werden neuste Web-Technologien wie Bootstrap CSS, Fluid Web Layouts und Lazy-Loading zum Einsatz kommen.

Online-Shops sind in zwei wesentliche Bereiche getrennt: Frontend und Händler Online-Verwaltung. Das Frontend ist für jeden Kunden öffentlich erreichbar, hier befinden sich die Produktlistings, Produkt-Kategorien und der Warenkorb mit der Oberfläche zum Kaufabschluss.

## Händler Online-Shops



### Produktlistings

Produktlistings sind zugängliche Auflistungen

#### Filter



## Händler Online-Verwaltung

Die Händler Online-Verwaltung ist eine Webseiten-Oberfläche, in der Händler Ihren Online-Shop konkret verwalten kann. Hierfür steht im ein passwortgeschützter Bereich in seinem Online-Shop zur Verfügung, auf den nur er Zugriff hat.

### Übersicht

In der Online-Shop Übersicht wird der Händler konkrete Umsätze und relevante Statistiken zu seinem Online-Shop einsehen können. Zudem werden hier die Provisionsabrechnungen zu seinem Online-Shop aufgeführt.

### Produkte

Dem Händler wird eine übersichtliche, grafische Oberfläche zur Verfügung gestellt, auf der er seine Produkte einstellen und verwalten kann.

### Bestellungen

In der Händler-Verwaltung können eingegange Bestellungen eingesehen und verwaltet werden. Zu jeder Bestellung wird der Händler fertige Rechnungen, Lieferscheine, Rücksendeformulare und Retouren im PDF-Format herunterladen können.

Bestellungen können Zurückerstattet und Storniert werden.

### Statische Unterseiten

Statische Unterseiten sind Informationsseiten, welche dazu dienen den Kunden seine Kaufentscheidung zu erleichtern und Vertrauen aufzubauen. Unterseiten können vom Händler selbstständig und frei angelegt und gestaltet werden. Voreingestellt sind Unterseiten für Rechtstexte, welche für den Betrieb eines Online-Shops notwendig sind.

Statische Unterseiten können vom Händler über einen Online-Page-Builder selbstständig angelegt und verwaltet werden, HTML Kenntnisse sind nicht notwendig um ansprechende Layouts erstellen zu können.

#### Seitengenerator

Den Händlern werden Online Page-Builder entwickelt, um die optische Erscheinung der Unterseiten nach seinen Wünschen anzupassen.

#### Über uns Seite

Die Über-uns Seite wird mit Informationen aus dem STILPUNKTE Lifestyle Online-Portal automatisch erstellt. Angezeigt werden: Einleitungstext, Öffnungszeiten, Social Media Verknüfungen, Adresse.

#### Kontaktseite

Dem Händler wird ein professionelles Tool zur Seite gestellt, in dem er Kontaktformulare nach seinen Ansprüchen per Drag & Drop erstellen kann. Kontaktformulare können auf statischen Seiten und Produkten eingebunden werden. Kunden können diese Formulare nutzen um mit dem Händler in Kontakt zu treten. Ausgefüllte Kontaktformulare versehen eine Email an den Händler.

### Blog

Erstellt wird ein Tool zur Verwaltung eines Online-Blogs auf dem Online-Shop des Händlers.

Es können Blog-Kategorien erstellt werden. Blog-Einträge wird man über Schaltflächen dirket auf Social-Media Kanälen teilen.

### Kategorien

Der Händler kann Produkt-Kategorien verwalten und pflegen. Diese sind im Online-Shop automatisch angezeigt und es können SEO-Texte, sowie Hero-Fotos hochgeladen eingebunden werden.

Den Händlern werden Online Page-Builder entwickelt, um die optische Erscheinung der Kategorie nach seinen Wünschen anzupassen.

### Marketing

Den Händlern werden Marketing-Tools entwickelt um die Conversion seines Online-Shops professionell zu unterstützen:

#### Rabatt-Codes

Dem Händler wird es frei stehen, seinen Kunden Rabatt-Codes zur Verfügung zu stellen. Diese senken den Warenkorb-Wert prozentual oder um einen bestimmten Betrag.

Der Händler kann Rabatt-Codes frei erstellen und verwalten. Rabatt-Codes können im Warenkorb des Online-Shops eingelöst werden und wirken sich sofort auf den Warenkorb aus.

#### Newsletter-Versand

Dem Händler wird ein professionelles Tool zur Seite gestellt, in dem er Newsletter an seine Kunden versenden kann. Mit diesem Tool wird der Händler selbst per Drag & Drop und einer Auswahl an Templates Newsle

#### Retargeting per Mail

Sollte ein Kunde seinen Warenkorb nicht abgeschlossen haben, senden wir dem Kunden nach 12 Stunden eine Erinnerungsmail. Dies soll die Abschlussrate erhöhen.

Dieser Automatismus kann vom Händler selbst eingestellt werden.

### Einstellungen

Händler werden Ihren Online-Shop in einer einfachen, grafischen Online-Oberfläche selbstständig einrichten und verwalten können.

- **Farben anpassen**  
  Menü, Hintergrund und Footer Farben können angeapsst werden.
- **Logos**  
  Der Kunde hat die Möglichkeit seine Logos hochzuladen, welche im Menü, im Footer und auf Rechnungen angezeigt werden.
- **Rechnungsinformationen**  
  Vorgefertigte Formulare für Rechnungsinformationen helfen den Händlern alle notwendigen Informationen einzutragen, welche für einen Rechtssicheren Betrieb eines Online-Shops erforderlich sind.
- **Zahlungseinstellungen**  
  Händler können alle Zahlungsmethoden für Ihren Online-Shop selbst eintragen. Somit findet der Verkauf direkt unter dem Händler und dem Kunden statt, ohne das die Promediagroup dort involviert wurde.
- **Versand & Zustellung**  
  Händler können Versandoptionen ihren Kunden anbieten, um den Versand nach Deutschland, in die Europäische Union und Weltweit zu garantieren.  
  Händler können auch eine lokale Abholung anbieten.

#### Versand

Händlern steht es frei Bestellungen von Kunden eine Tracking-Nummer hinzuzugügen, damit Kunden Ihre Sendung nachverfolgen können. Zudem werden automatisierte Emails ausgelöst, welche den Kunden über das Tracking informieren.

#### Zahlungsanbindungen

Folgendde Zahlungsmethoden werden den Händlern angeboten:

- Paypal
- Stripe (Kreditkartenzahlung)
- Klarna

#### Mehrsprachigkeit

Händlern steht es frei ihre Produkte in diversen Sprachen zu übersetzen um einen weltweiten Verkauf zu gewährleisten.

#### Benutzerverwaltung

Händlern steht es frei zu Ihrem Account weitere Benutzer hinzuzufügen um Mitarbeitern die Verwaltung des Online-Shops zu ermöglichen.

#### Einbindung Trusted Shops

Dem Händlern wird die Möglichkeit gegeben seinen Trusted Shops Account mit seinem Online-Shop zu verbinden. Hierdurch soll ein größeres Vertraue

#### Bilder-Upload zu AWS S3

Das System wird so vorbereitet, dass vom Kunden hochgeladene Daten (Fotos, PDF, Logos, etc...) automatisch auf einen AWS S3 Server übertragen werden, um die Skalierbarkeit des kompletten Projekts zu optimieren.

## Integration in STILPUNKTE®

Vom Händler eingestellte Produkte werden als weitere Werbemöglichkeit auch auf dem STILPUNKTE® Online Lifestyle Guide gelistet und können dort angesehen werden. Über einen Link zum Online-Shop gelangen Kunden automatisch auf den Online-Shop des Händlers, auf dem der Kaufprozess abgeschllssen wird.

## Provisionsabrechnung

Das System wird automatisch Provisionsabrechnungen im PDF-Format in einem zweiwöchigem Rythmus erstellen, welche an die Händler und an die Buchhaltung der Promediagroup per Email verschickt werden. Provisionsabrechnungen werden anhand von Verkäufen im Online-Shop pro Händler berechnet. Pro Verkauf im Online-Shop ist ein Teil des Verkaufsbetrags an Promediagroup abzuführen, der erhebbare Teil ist für jeden Händler individuall einstellbar. Die Provisionsabrechnung fasst alle Verkäufe aus dem oben genannten zweiwöchigem Rythmus zusammen.

Stornierte Bestellungen werden in den Provisionsabrechnungen einzeln aufgeführt und reduzieren den abzurechnenden Provisionsbetrag.

Provisionsabrechnungen können in der Händler-Verwaltung angesehen und heruntergeladen werden.

Alle Provisionsabrechnungen können in der Administrator-Verwaltung angesehen und heruntergeladen werden.

## SEO Optimierung

Der Online-Shop Marktplatz, sowie die Online-Shops werden nach aktuellen Standards für Suchmaschinen optimiert.

## Online-Dokumentation

Erstellt wird eine Online-Dokumentation für Händler, die den Einstieg in den Online-Shop vereinfacht und alle Bereiche einzeln erklärt. Diese ist auf einer Webseite erreichbar und wird in der Händler-Verwaltung verlinkt.

## STILPUNKTE® App
