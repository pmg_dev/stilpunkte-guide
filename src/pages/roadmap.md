---
meta:
  - name: robots
    content: noindex, nofollow
---
# Roadmap

## STILPUNKTE Frontend

- [x] Newsletter Anmeldung optisch aufwerten
- [ ] Fotos zu AWS S3 kopieren
  - [x] blog
  - [x] brands
  - [x] galleries_pictures
  - [x] news
  - [x] news_pictures
  - [x] offers
  - [x] partners
  - [x] partners_pictures
  - [ ] products
  - [ ] products_pictures
  - [x] product_topics
  - [x] recipe
  - [x] recipe_advice
  - [x] recipe_menus
  - [ ] shopping_category
  - [x] travelog_pictures
- [x] Partner Details: WhatsApp als Kontaktinformation
- [x] CMS für statische Unterseiten
- [x] Partner Bewertungskarten

## STILPUNKTE Backend

- [ ] Newsletter-Generator neu konzeptionieren
- [ ] Rechnung stornieren, doppelt fragen

### Administrator Statistiken

- [x] Shops
  - [ ] Produkte pro Kategorie
  - [ ] Durschnittspreis Produkt pro Kategorie / Themenbereich / Partner

## STILPUNKTE Shopping-Mall

- [ ] Auswertung/Analysen von AdSense von Produkten/Shops
- [ ] Suche bei `Produkte`
- [ ] Filter-Funktion
- [ ] Cart Drawer
- [x] Landingpage zum Erstellen des eigenen Shops
- [ ] Footer: Verlinkung Newsletter-Anmeldung
- [x] STILPUNKTE Shop (alle kaufbaren Produkte in ansprechendem Layout)
- [x] Suche
- [x] Desktop Ansicht
  - [x] Sales
  - [x] Produkt-Detail
- [x] Mobile Ansicht
  - [x] Shopping-Mall
  - [x] Händler
  - [x] Produkte
  - [x] Produkt-Detail
  - [x] Sales
  - [x] Warenkörbe
  - [x] Kategorien

## Partner Online-Shop

- [ ] Payment: Klarna
- [ ] SALE Kategorie
- [ ] Zugriffsstatistiken
- [ ] Tracking-Nummer hinzufügen
  - [ ] Automatische Email an den Kunden über das Tracking
- [x] Produkt-Varianten
- [ ] (Frontend) Produkt-Filter in Kategorien
- [ ] Startseite Stylen können
- [x] Rabatte
- [ ] Blog
- [ ] Multi-Language
- [ ] Benutzer-Verwaltung (Zusätzliche Benutzer hinzufügen)
- [ ] Retargeting per Mail nach 12 Stunden (wenn Kunde Warenkorb nicht abgeschlossen hat)
- [ ] Trusted Shops
- [ ] Bilder auf AWS für bessere Performence
- [ ] Produkte bewerten
- [x] Instagram-Produkte verlinken
- [x] Newsletter-Button vor Kaufabschluss
  - [ ] "Ich möchte vom Händler und seinen Kooperationspartnern zukünftige Newsletter erhalten."
- [ ] automatischer Versand neuer Rechnugen bei Stornierung und Rückerstattung
- [ ] zweite Bestätigung bei `Produkt löschen`
- [ ] Ergänzung Hinweis Mindestanzahl Zeichen bei META Beschreibung
- [ ] Footer: Möglichkeit weitere Badgets/Auszeichnung hinterlegen zu können
- [ ] Footer: Verlinkung Newsletter-Anmeldung
- [x] Paypal und Stripe als Pflicht bevor der Shop vom Kunden freigeschaltet werden kann
- [x] Kontakt-Seite
- [x] Shop Footer: STILPUNKTE (+ Logo), STILPUNKTE Online Shop
- [x] Shop Banner/Link im Eintrag
- [x] Cart/Checkout Mobil optimieren
- [x] Share individuell pro Partner
- [x] Cart Icon in der Menü-Zeile
- [x] Suche
- [x] Entry Backend
- [x] Kategorie Bild löschen

## Instagram Feed

- [Facebook Commerce Manager](https://www.facebook.com/commerce_manager/)
- [Facebook Products](https://www.facebook.com/products/)
- [Facebook Business](https://www.facebook.com/business/help/125074381480892?id=725943027795860)
- [How to import Products to facebook](https://faq.businesstech.fr/en/faq/228-how-to-import-my-products-into-a-facebook-catalog)
