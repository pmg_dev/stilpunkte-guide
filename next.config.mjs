import withMarkdoc from "@markdoc/next.js";

import withSearch from "./src/markdoc/search.mjs";

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  pageExtensions: ["js", "jsx", "md", "jpg"],
  images: {
    unoptimized: true,
  },
  output: "export",
  experimental: {
    scrollRestoration: true,
  },
};

export default withSearch(
  withMarkdoc({ schemaPath: "./src/markdoc" })(nextConfig)
);
